# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 6.3.0 (2023-12-07)

- !214 update to v3 Raytio APIs for instances
- !211, !212, !213 automated dependency updates

## 6.2.0 (2023-07-17)

- !210 update to v2 Raytio APIs
- !208 automated dependency updates

## 6.1.1 (2023-06-28)

- !203 fix issue with the npm publish process
- !203 automated dependency updates

## 6.1.0 (2023-06-27)

- !206 support videos in the PDF
- !207 code quality improvements

## 6.0.0 (2023-04-21)

- !197 💥 BREAKING CHANGE: Require node v18 or later
- !197 update API for fetching schema
- !198, !199, !200, !201, !205 automated dependency updates

## 5.1.3 (2022-11-11)

- !196 fix more issues caused by corrupted images
- !192, !194 automated dependency updates

## 5.1.2 (2022-11-10)

- !195 retry API requests that fail with a 504 error
- !195 fix crash from corrupted binary files

## 5.1.1 (2022-08-15)

- !189 load verifier details faster
- !187 automated dependency updates

## 5.1.0 (2022-08-13)

- !188 update raytio/core so that `repairDate` is used on dates
- !186 automated dependency updates

## 5.0.0 (2022-07-12)

- 💥 BREAKING CHANGE: !185 use `accessToken` instead of `idToken` when communicating with the Raytio API
- 💥 BREAKING CHANGE: !185 Improve how quotes (`"`) are escaped in the csv format to be compliant with [RFC-4180](https://datatracker.ietf.org/doc/html/rfc4180#section-2)
- !185 embed language into pdf
- !184 automated dependency updates

## 4.0.0 (2022-07-08)

- 💥 BREAKING CHANGE: !170 [json] [csv] changed the format of badges, the fields `$verified` and `$safeHarbour` have been replaced with `$badges`
- !176 fix field values being unexpectedly transformed
- !179 update dependencies
- !182 save the calculated score to the instance

## 3.2.0 (2022-04-21)

- !177 do not use `hashPassword` when logging in, and migrate old credentials

## 3.1.1 (2022-04-02)

- !175 Fix import issue in v3.1

## 3.1.0 (2022-03-14)

- !168 Support internationalizing the pdf
- !167 Support while labelling the pdf
- !166 List the person schema first in the PDF
- !163, !165, !169, !171, !172 automated dependency updates

## 3.0.2 (2022-02-16)

- !164 fix PDF bug for multiple POs from the same schema
- !164 remove legacy internal property called `$source`
- !164 fix field verification status being `undefined` instead of `NotVerified`/`60002`
- 💥 BREAKING CHANGE: we no longer use `schema_group` to group schema in the json, csv, and pdf

## 3.0.1 (2022-02-15)

- !162 fix bug with PDF generation

## 3.0.0 (2022-02-04)

- 💥 BREAKING CHANGE: renamed the `USERNAME` & `PASSWORD` environment variables to `RAYTIO_USERNAME` & `RAYTIO_PASSWORD`
- 💥 BREAKING CHANGE: removed the `READABLE_FIELD_NAMES` option
- 💥 BREAKING CHANGE: [csv] added new columns, shifted other columns
- 💥 BREAKING CHANGE: [json] removed some duplicate legacy fields, see #33
- !156: various internal refactoring

## 2.2.1 (2022-02-09)

- !158 include the submission score in the json and pdf

## 2.2.0 (2022-02-04)

- !153 fix rendering of arrays of primitive values
- !152 Update @raytio/core to v9 to fix a bug with `hashed_n_id`
- !155, !154, !146, !147, !150, !151 automated dependency updates

## 2.1.3 (2021-10-08)

- !134 Add more logging to `saveToS3Bucket`
- !129, !131, !132, !133 automated dependency updates

## 2.1.2 (2021-09-24)

- !129 Update @raytio/core to v8.1.2 to support the new API format for `hashed_n_id`

## 2.1.1 (2021-09-15)

- !126 Update @raytio/core to v8.1.1 to fix a bug with `hashed_n_id`

## 2.1.0 (2021-09-03)

- !117 Update @raytio/core to v8
- !120 Render document validation score

## 2.0.0 (2021-06-28)

- 💥 BREAKING CHANGE: requires nodejs v14 or newer
- 💥 BREAKING CHANGE: dates in the PDF will now use the `DATE_FORMAT` and `TIMEZONE` environment variables, which were previously required, but not used due to a bug in node v12.
- removed various polyfills for node12, which reduced the install size by 26MB

## 1.4.1 (2021-05-21)

- !103 embed decrypt-helper version into PDF metadata
- !101, !100, !99, !98, !97, !96, !94, !93, !87 automated dependency updates

## 1.4.0 (2021-04-27)

- !95 release
- !92 remove base64 of encrypted images from json output
- !91 use schema lookups in the pdf report
- !90 only show verification badges in the pdf report if the the data is verifiable
- !89 automated dependency updates

## 1.3.4 (2021-04-15)

- !86 Fix first row of table being simulating table header on page break when table header isn't specified.
- !85 update maxcryptor
- !75 refresh lockfile

## 1.3.3 (2021-04-14)

- !84 minor refactoring

## 1.3.2 (2021-04-13)

- !80 update raytio packages
- !77, !78, !82 automated dependency updates

## 1.3.1 (2021-03-21)

- !73 fixes a bug introduced in `1.3.0` where the content type of images from `urn:temp_object:` was wrong

## 1.3.0 (2021-03-21)

- !72 handle images from `urn:temp_object:`
- !71 automated dependency updates

## 1.2.1 (2021-03-17)

- !70 improve fetchEnvConfig debugging
- !68 tweak renovate config
- !64, !69 automated dependency updates

## 1.2.0 (2021-03-10)

- !67 support encrypted images
- !60, !66 automated dependency updates

# 1.1.1 (2021-03-03)

- !63 upgrade raytio packages
- !61, !62 automated dependency updates

## 1.1.0 (2021-02-26)

- !57 release
- !55 fix bug
- !51 support arrays of objects in PDF
- !32 add screenshot tests for PDF generation
- !20, !21, !22, !23, !24, !25, !27, !28, !29, !30, !33, !36, !38, !39, !41, !43, !45, !46, !49, !50, !54 automated dependency updates
- !19, !53 set up automated dependency updates

## 1.0.1 (2021-01-27)

- !18 fix: fix bug in PDF generation

## 1.0.0 (2021-01-21)

- versioning now follow semver. This update in itself is not a breaking change
- !16 fix: handle corrupted submission data

## 0.0.10 (2021-01-21)

- !15 chore: enable more logging if processing a submission fails
- !15 chore: update raytio packages

## 0.0.9 (2021-01-09)

- !14 fix: show a nicer error if an invalid clientUrl is supplied

## 0.0.8 (2021-01-08)

- !12 feat(pdf): add Safe Harbour badge to pdf
- !12 feat(json): add more details to json data (`$nId`, `$schemaName`, `$properties`)

## 0.0.7 (2021-01-07)

- !13 feat: export the `fetchEnvConfig` function

## 0.0.6 (2020-12-16)

- !11 fix: update @raytio/core to fix verification bugs

## 0.0.5 (2020-11-20)

- !10 fix: update @raytio/core to fix bug
- !9 feat: use schema groups

## 0.0.4 (2020-11-13)

- !8 fix(pdf): change hard to read green text
- !8 chore: update dependencies

## 0.0.3 (2020-10-16)

- !7 feat(`saveToS3Bucket`): store data in folder based on `i_id`.

## 0.0.2 (2020-10-09)

First stable release
