import { fileURLToPath } from "node:url";
import { dirname } from "node:path";

export function getDirname() {
  const __filename = fileURLToPath(import.meta.url);
  return dirname(__filename);
}
