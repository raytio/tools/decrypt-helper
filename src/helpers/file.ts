import { isEncrypted } from "@raytio/core";
import type { Encrypted, NId, Urn } from "@raytio/types";
import type { FlatPO } from "./formatOutput.js";

const URN_PREFIX = "urn:";

/** @internal */
const isObject = (x: unknown): x is Record<string, unknown> =>
  !!x && typeof x === "object";

/** @internal */
export const isTruthy = <T>(x: T | undefined | null | 0 | ""): x is T => !!x;

/** @internal */
export const isUrn = (text: string): text is Urn => text.startsWith(URN_PREFIX);

/** @internal */
const isUrnOrEncrypted = (x: unknown): x is Urn | Encrypted =>
  (typeof x === "string" && x.startsWith(URN_PREFIX)) || isEncrypted(x);

export type RaytFile =
  | { n_id: NId; content: Urn | Encrypted }
  | { n_id: NId; Content: Urn | Encrypted };

/** checks if a field value is a file */
export const isFieldValueFile = (fieldValue: unknown): fieldValue is RaytFile =>
  isObject(fieldValue) &&
  "content" in fieldValue &&
  isUrnOrEncrypted(fieldValue.content);

/** checks if a PO is a file */
export const isPOFile = (PO: FlatPO) =>
  isFieldValueFile({ content: PO.$properties.content?.value });
