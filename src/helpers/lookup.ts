import type { Schema } from "@raytio/types";
import { getLookupOption } from "../api/getLookupOption.js";

export const maybeGetLookupValue = async <T>(
  schema: Schema,
  k: string,
  value: T,
  apiToken: string
): Promise<T | string | number | undefined> => {
  const lookupUrl = schema.properties?.[k]?.lookup;
  if (!lookupUrl) return undefined;

  // Replace environment variables in the lookup URL
  const regex = /{([^}]+)}/g;
  const matches = lookupUrl.match(regex);

  const replaced = matches?.map((match) => {
    const toReplace = match.replace("{", "").replace("}", "");
    return process.env[toReplace] ?? "";
  });

  const resolvedLookupUrl = matches
    ? matches
        .reduce(
          (url, match, i) => url.replace(match, replaced?.[i] ?? ""),
          lookupUrl
        )
        .replaceAll(/([^:]\/)\/+/g, "$1") // replaces doubles slashes (bar http://)
    : lookupUrl;

  const lookup = await getLookupOption(apiToken, resolvedLookupUrl);
  return lookup.find((l) => l.key === (value as unknown))?.value;
};
