export function assertSafeProperty(
  property: unknown
): asserts property is string {
  if (typeof property !== "string" || property in {}) {
    throw new Error(`Invalid property name '${property}'`);
  }
}
