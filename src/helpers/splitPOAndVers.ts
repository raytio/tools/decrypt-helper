import { findSchemaLabel } from "@raytio/core";
import type { ProfileObject, Verification } from "@raytio/types";
import { SCHEMA } from "../constants.js";

export const splitPOAndVers = (
  list: ProfileObject[]
): [ProfileObject[], Verification[]] =>
  list.reduce<[ProfileObject[], Verification[]]>(
    (accumulator, PO) => {
      const isVerification = findSchemaLabel(PO.labels) === SCHEMA.VERIFICATION;

      // move to the left or right array depending on whther +isVerification is 0 or 1
      accumulator[+isVerification]!.push(PO as Verification);
      return accumulator;
    },
    [[], []]
  );
