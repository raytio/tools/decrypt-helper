import { getLookupOption } from "../../api/getLookupOption.ts";
import { maybeGetLookupValue } from "../lookup.ts";

vi.mock("../../api/getLookupOption.ts");

describe("maybeGetLookupValue", () => {
  it("returns undefined if there is no lookup", async () => {
    getLookupOption.mockImplementation(() => []);
    const value = "NZ";
    const key = "country";
    const schema = {
      properties: {
        country: {
          priority: 200,
          tags: ["display:no_autofill"],
          description: "The country of this location",
          default: "NZ",
          type: "string",
          title: "Location country",
        },
      },
    };

    const result = await maybeGetLookupValue(schema, key, value);
    expect(result).toBeUndefined();
  });

  it("Returns lookup value", async () => {
    getLookupOption.mockImplementation(() => [
      { key: "NZ", value: "New Zealand" },
    ]);
    const value = "NZ";
    const key = "country";
    const schema = {
      properties: {
        country: {
          priority: 200,
          tags: ["display:no_autofill"],
          lookup: "https://LookupAPI.yes",
          description: "The country of this location",
          default: "NZ",
          type: "string",
          title: "Location country",
        },
      },
    };

    const result = await maybeGetLookupValue(schema, key, value);
    expect(result).toBe("New Zealand");
  });

  it("Returns value if no corresponding key in lookup", async () => {
    getLookupOption.mockImplementation(() => [
      { key: "DE", value: "Doesn't Exist" },
    ]);
    const value = "NZ";
    const key = "country";
    const schema = {
      properties: {
        country: {
          priority: 200,
          tags: ["display:no_autofill"],
          lookup: "https://LookupAPI.yes",
          description: "The country of this location",
          default: "NZ",
          type: "string",
          title: "Location country",
        },
      },
    };

    const result = await maybeGetLookupValue(schema, key, value);
    expect(result).toBeUndefined();
  });
});
