import {
  FieldVerification,
  type NId,
  POVerification,
  type ProfileObject,
  type RealVer,
  type Schema,
  type SchemaName,
} from "@raytio/types";
import { formatOutput } from "../formatOutput";
import { resolveVerificationDetails } from "../../api";

vi.mock("../../api");

const profileObjects: ProfileObject[] = [
  {
    labels: ["ss_name"],
    properties: { fName: "john", lName: "doe" },
    n_id: <NId>"n1",
  },
  {
    labels: ["ss_nz_drivers_license"],
    properties: { license_number: "DW012345", issuer: "NZTA" },
    n_id: <NId>"n2",
  },
  {
    labels: ["ss_nsw_drivers_license"],
    properties: { license_number: "ABC123", city: "Sydney" },
    n_id: <NId>"n3",
  },
];

const allSchemas: Schema[] = [
  {
    name: <SchemaName>"ss_name",
    title: "Your Name",
    description: "",
    properties: {},
    version: "0.0.0",
  },
  {
    name: <SchemaName>"ss_nz_drivers_license",
    schema_group: "DL",
    title: "NZ Drivers License",
    description: "",
    properties: {},
    version: "0.0.0",
  },
  {
    name: <SchemaName>"ss_nsw_drivers_license",
    schema_group: "DL",
    title: "NSW Drivers License",
    description: "",
    properties: {},
    version: "0.0.0",
  },
];
const realVers: RealVer[] = [];

describe("formatOutput", () => {
  beforeEach(() => {
    m(resolveVerificationDetails).mockResolvedValue(
      Symbol.for("VerificationDetails")
    );
  });

  it("does not use schema_group to group schema", async () => {
    const output = await formatOutput(
      profileObjects,
      allSchemas,
      realVers,
      "[apiToken]",
      // @ts-expect-error testing if it works with booleans not stringied bools
      Symbol.for("envConfig")
    );

    expect(output).toStrictEqual({
      ss_nz_drivers_license: [
        {
          $verification_details: Symbol.for("VerificationDetails"),
          $badges: {
            safeHarbour: undefined,
            verified: {
              code: POVerification.NotVerified,
              statusText: "Not Verified",
            },
          },
          $shouldBeVerifiedFields: undefined,
          $nId: "n2",
          $properties: {
            issuer: {
              title: "issuer",
              value: "NZTA",
              verification: FieldVerification.NotVerified,
            },
            license_number: {
              title: "license_number",
              value: "DW012345",
              verification: FieldVerification.NotVerified,
            },
          },
          $schemaName: "ss_nz_drivers_license",
          $schemaTitle: "NZ Drivers License",
        },
      ],
      ss_nsw_drivers_license: [
        {
          $verification_details: Symbol.for("VerificationDetails"),
          $badges: {
            safeHarbour: undefined,
            verified: {
              code: POVerification.NotVerified,
              statusText: "Not Verified",
            },
          },
          $shouldBeVerifiedFields: undefined,
          $nId: "n3",
          $properties: {
            city: {
              title: "city",
              value: "Sydney",
              verification: FieldVerification.NotVerified,
            },
            license_number: {
              title: "license_number",
              value: "ABC123",
              verification: FieldVerification.NotVerified,
            },
          },
          $schemaName: "ss_nsw_drivers_license",
          $schemaTitle: "NSW Drivers License",
        },
      ],
      ss_name: [
        {
          $verification_details: Symbol.for("VerificationDetails"),
          $badges: {
            safeHarbour: undefined,
            verified: {
              code: POVerification.NotVerified,
              statusText: "Not Verified",
            },
          },
          $shouldBeVerifiedFields: undefined,
          $nId: "n1",
          $properties: {
            fName: {
              title: "fName",
              value: "john",
              verification: FieldVerification.NotVerified,
            },
            lName: {
              title: "lName",
              value: "doe",
              verification: FieldVerification.NotVerified,
            },
          },
          $schemaName: "ss_name",
          $schemaTitle: "Your Name",
        },
      ],
    });
  });
});
