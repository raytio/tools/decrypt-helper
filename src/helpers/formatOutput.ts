import {
  calcSafeHarbourScore,
  findSchemaLabel,
  getPOVerification,
} from "@raytio/core";
import { omit } from "ramda";
import {
  FieldVerification,
  type NId,
  type POVerification,
  type ProfileObject,
  type RealVer,
  type Schema,
} from "@raytio/types";
import { $$ } from "../locales/index.js";
import {
  type EnvConfig,
  type ResolvedVerificationProvider,
  resolveVerificationDetails,
} from "../api/index.js";
import { FIELDS_TO_REMOVE, PO_VER_TEXT_MAP, SCHEMA } from "../constants.js";
import { maybeGetLookupValue } from "./lookup.js";
import { assertSafeProperty } from "./types.js";

export type FlatPO = {
  $verification_details: ResolvedVerificationProvider | undefined;
  $shouldBeVerifiedFields: string[] | undefined;
  $nId: NId;
  $schemaName: string;
  $schemaTitle: string;
  $properties: {
    [fieldName: string]: {
      title: string;
      value: unknown;
      formatted_value?: string | number;
      verification: FieldVerification;
    };
  };
  $badges: {
    verified: { code: POVerification; statusText: string };
    safeHarbour?: { code: boolean; statusText: string };
  };
};

const byPriority =
  (schema: Schema) =>
  (
    [a]: [fieldName: string, fieldValue: unknown],
    [b]: [fieldName: string, fieldValue: unknown]
  ) => {
    assertSafeProperty(a);
    assertSafeProperty(b);
    // if no priority, it becomes the last
    const aPriority = schema.properties?.[a]?.priority ?? Infinity;
    const bPriority = schema.properties?.[b]?.priority ?? Infinity;
    return aPriority - bPriority;
  };

export async function formatOutput(
  profileObjects: ProfileObject[],
  allSchemas: Schema[],
  realVers: RealVer[],
  apiToken: string,
  envConfig: EnvConfig
): Promise<Record<string, FlatPO[]>> {
  const PODetails = profileObjects.reduce<Promise<Record<string, FlatPO[]>>>(
    async (accumulatorPromiseOuter, PO) => {
      const accumulatorOuter = await accumulatorPromiseOuter;
      const schemaName = findSchemaLabel(PO.labels) as string;
      const schema = allSchemas.find((x) => x.name === schemaName);
      if (!schema) throw new Error(`${schema} is missing!`);
      const versionDetails = getPOVerification({ PO, schema, realVers });

      const realProperties = omit(FIELDS_TO_REMOVE, PO.properties);

      assertSafeProperty(schemaName);

      const existing = accumulatorOuter[schemaName] || [];
      const poProperties = Object.entries(realProperties).sort(
        byPriority(schema)
      );
      const reducedProperties = await poProperties.reduce<
        Promise<FlatPO["$properties"]>
      >(async (accumulatorPromiseInner, [key, value]) => {
        const accumulatorInner = await accumulatorPromiseInner;
        assertSafeProperty(key);

        const prettyValue =
          typeof value === "string"
            ? await maybeGetLookupValue(schema, key, value, apiToken)
            : undefined;

        const POInfo: FlatPO["$properties"][string] = {
          title: schema.properties?.[key]?.title || key,
          value,
          verification:
            versionDetails.fieldVerifications[key] ||
            FieldVerification.NotVerified,
        };
        if (prettyValue) POInfo.formatted_value = prettyValue;

        return { ...accumulatorInner, [key]: POInfo };
      }, Promise.resolve({}));

      const thisPO: FlatPO = {
        $verification_details: await resolveVerificationDetails(
          versionDetails.details.verifiers,
          envConfig
        ),
        $shouldBeVerifiedFields: schema.verified_fields?.map((x) =>
          typeof x === "string" ? x : x.field
        ),
        $nId: PO.n_id,
        $schemaName: schemaName,
        $schemaTitle: schema.title,
        $properties: reducedProperties,

        $badges: {
          verified: {
            code: versionDetails.status,
            statusText: PO_VER_TEXT_MAP[versionDetails.status],
          },
          safeHarbour:
            schemaName === SCHEMA.PERSON
              ? await calcSafeHarbourScore({
                  person: PO,
                  getSchema: async (name) =>
                    allSchemas.find((x) => x.name === name)!,
                  profileObjects,
                  realVers,
                }).then(({ isSafe }) => ({
                  code: isSafe,
                  statusText: isSafe
                    ? $$("POVerificationBadge.safe-harbour-yes")
                    : $$("POVerificationBadge.safe-harbour-no"),
                }))
              : undefined,
        },
      };

      return {
        ...accumulatorOuter,
        [schemaName]: [...existing, thisPO],
      };
    },
    Promise.resolve({})
  );

  return PODetails;
}
