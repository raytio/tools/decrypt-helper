import * as maxcryptor from "@raytio/maxcryptor";
import { assocPath } from "ramda";
import type { ICognitoUserAttributeData } from "amazon-cognito-identity-js";
import { ATTRIBUTE_MAP, type Config } from "../constants.js";

const { encryptorFromExistingUser } = maxcryptor;

const isMaxcryptorAttribute = (
  name: string
): name is keyof typeof ATTRIBUTE_MAP => name in ATTRIBUTE_MAP;

export async function setupMaxcryptor(
  CONFIG: Config,
  cognitoAttributes: ICognitoUserAttributeData[]
): Promise<maxcryptor.Maxcryptor> {
  const userDocument = cognitoAttributes.reduce(
    (accumulator, { Name, Value }) => {
      if (!isMaxcryptorAttribute(Name)) return accumulator;
      return assocPath(ATTRIBUTE_MAP[Name], JSON.parse(Value), accumulator);
    },
    {} as maxcryptor.UserDoc
  );

  const { encryptor: maxcryptorEncryptor } = await encryptorFromExistingUser(
    userDocument,
    CONFIG.RAYTIO_PASSWORD
  );
  return maxcryptorEncryptor;
}
