/* eslint-disable @typescript-eslint/no-explicit-any */
import { chain, fromPairs, map, toPairs, transpose, type } from "ramda";

// copied from raytio-client
export const flattenObject = (
  rootObject: Record<string, unknown>
): Record<string, never> => {
  const go = (object: any): any =>
    chain<any, any, unknown>(([k, v]) => {
      if (type(v) === "Object" || type(v) === "Array") {
        return map(([k_, v_]) => [`${k}.${k_}`, v_], go(v));
      }
      return [[k, v]];
    }, toPairs(object));

  return fromPairs(go(rootObject));
};

const toCsvRow = (row: string[]): string =>
  row
    .map((field) =>
      typeof field === "string" ? `"${field.replaceAll('"', '""')}"` : field
    )
    .join(",");

export function deepJsonToCsv(json: Record<string, unknown>): string {
  const flatJson = flattenObject(json);
  const [headerRow, values] = transpose(Object.entries(flatJson));

  return `${toCsvRow(headerRow!)}\n${toCsvRow(values!)}`;
}
