// eslint-disable-next-line @typescript-eslint/consistent-type-imports
import * as RaytioCore from "@raytio/core";
import {
  FieldVerification,
  type Instance,
  POVerification,
} from "@raytio/types";
import { $$ } from "./locales/index.js";

export const ATTRIBUTE_MAP = {
  "custom:kek_derivation": ["kek_derivation_config"],
  "custom:dek_encryption": ["private_key_encryption_config"],
  "custom:aek_public": ["encryption_key_pair", "public_key"],
  "custom:aek_private": ["encryption_key_pair", "private_key"],
  "custom:ask_public": ["signing_key_pair", "public_key"],
  "custom:ask_private": ["signing_key_pair", "private_key"],
};

export const INSTANCE_FIELDS_TO_REMOVE = [
  "profile_objects",
  "relationships",
  "keys",
  "score",
] as const;
export const FIELDS_TO_REMOVE = ["n_id", "document"] as const;

export const ENV_VARIABLES = [
  "CLIENT_URL",
  "RAYTIO_USERNAME",
  "RAYTIO_PASSWORD",
] as const;

export type Config = Record<(typeof ENV_VARIABLES)[number], string>;

export type InstanceDataToPassOn = Omit<
  Instance,
  (typeof INSTANCE_FIELDS_TO_REMOVE)[number]
> & { score?: RaytioCore.ScoreResult };

export const PO_VER_TEXT_MAP: Record<POVerification, string> = {
  [POVerification.FullyVerified]: $$("POVerification.FullyVerified"),
  [POVerification.PartiallyVerified]: $$("POVerification.PartiallyVerified"),
  [POVerification.NotVerified]: $$("POVerification.NotVerified"),
  [POVerification.Expired]: $$("POVerification.Expired"),
  [POVerification.VerifiedFalse]: $$("POVerification.VerifiedFalse"),

  // not possible but included here for completeness
  [POVerification.Encrypted]: $$("POVerification.Encrypted"),
  [POVerification.Loading]: $$("POVerification.Loading"),
};

export const FIELD_VER_TEXT_MAP: Record<FieldVerification, string> = {
  [FieldVerification.Verified]: $$("FieldVerification.Verified"),
  [FieldVerification.NotVerified]: $$("FieldVerification.NotVerified"),
  [FieldVerification.Expired]: $$("FieldVerification.Expired"),
  [FieldVerification.VerifiedFalse]: $$("FieldVerification.VerifiedFalse"),
};

export const SCHEMA = <const>{
  VERIFICATION: "ss_Verification",
  PERSON: "ss_Person_Name",
};
