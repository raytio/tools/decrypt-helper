import type { Encrypted } from "@raytio/types";

export type ApplicationEncryptorLike = {
  decrypt(data: Encrypted, wdek: string): unknown;
};

declare global {
  /** a useless wrapper just to easily tell typescript that this is a mocked function */
  function m(f: unknown): vi.Mock;

  namespace NodeJS {
    interface Global {
      m(f: unknown): vi.Mock;
    }
  }
}
