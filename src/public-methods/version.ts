import { readFileSync } from "node:fs";
import { join } from "node:path";
import { getDirname } from "../helpers/pathUtils.js";

export const packageDotJson = JSON.parse(
  readFileSync(join(getDirname(), "../../package.json"), "utf8")
);

export const version = Object.entries({
  LANG: process.env.PDF_LANGUAGE || "default",
  N: process.version.slice(1),
  DH: packageDotJson.version,
  C: packageDotJson.dependencies["@raytio/core"].slice(1),
  T: packageDotJson.dependencies["@raytio/types"].slice(1),
})
  .map((kv) => kv.join("="))
  .join(" ");
