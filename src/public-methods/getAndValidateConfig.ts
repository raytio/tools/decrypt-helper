import { type Config, ENV_VARIABLES } from "../constants.js";

/**
 * This helper is designed for environments where the configuration
 * options are in the environment variables, such as AWS lambda.
 * It extracts the relevant ENV variables and throws an error if
 * any are missing.
 */
export function getAndValidateConfig(): Config {
  const CONFIG = ENV_VARIABLES.reduce((accumulator, name) => {
    const value = process.env[name];
    if (!value) throw new Error(`${name} is not configured`);
    return { ...accumulator, [name]: value };
  }, {}) as Config;
  return CONFIG;
}
