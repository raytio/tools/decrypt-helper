import { equals, mapObjIndexed, omit } from "ramda";
import {
  type ScoreResult,
  calculateScore,
  convertInstanceToRuleInput,
  decryptSharedData,
  getMissingDataForInstance,
  getSomeoneElsesRealVerifications,
  isScoreConfigValid,
} from "@raytio/core";
import type { AId, IId, Instance, RealVer } from "@raytio/types";
import {
  type Config,
  FIELD_VER_TEXT_MAP,
  INSTANCE_FIELDS_TO_REMOVE,
  type InstanceDataToPassOn,
} from "../constants.js";
import {
  type FlatPO,
  deepJsonToCsv,
  formatOutput,
  setupMaxcryptor,
  splitPOAndVers,
} from "../helpers/index.js";
import {
  type EnvConfig,
  fetchAA,
  fetchEnvConfig,
  fetchInstanceData,
  getAllSchema,
  getFiles,
  signIn,
  updateInstanceData,
} from "../api/index.js";
import type { ApplicationEncryptorLike } from "../types.js";
import { NULL_I_ID } from "../pdf/constants.js";

type DecryptData = {
  apiToken: string;
  instance: Instance;
  encryptedInstance: Instance;
  applicationDecryptor: ApplicationEncryptorLike;
  realVers?: RealVer[];
};

export interface ProcessSubmissionInput {
  /** The a_id of the application */
  applicationId: AId;
  /** The i_id of this submission */
  instanceId: IId;
  /** whether to enable extra logging. Default = false */
  verbose?: boolean;
  /**
   * the configuration options. You can extract these from the environment
   * variables by using @see getAndValidateConfig
   */
  config: Config;

  /** You can optionally supply Instance data and an API token - for Raytio internal use */
  _supliedConfig?: DecryptData;
}

export interface ProcessSubmissionOutput {
  /** details of the submission, included the decrypted profile objects */
  json: InstanceDataToPassOn & {
    profile_objects: {
      [schemaName: string]: FlatPO[];
    };
  };
  /** A string of a CSV file containing the same data as in `json` */
  csv: string;
  /** an object of files that were attached to the submission, such as images */
  files: {
    [nId: string]: [dataUrl: string, fileExtension: string];
  };

  /** the a_id is passed on */
  a_id: AId;
  /** passed down so that pdf generation has access to it */
  client_url: string;
  /** passed down so that pdf generation has access to it */
  envConfig: EnvConfig;
}

async function decryptStage(
  log: typeof console.log,
  config: Config,
  envConfig: EnvConfig,
  instanceId: IId
): Promise<DecryptData> {
  log("Authenticating...");
  const { apiToken, cognitoAttributes } = await signIn(config, envConfig);

  log("Initializing decryptor...");
  const maxcryptor = await setupMaxcryptor(config, cognitoAttributes);

  log("Fetching instance (without keys or data)...");
  const instanceWithoutData = await fetchInstanceData(
    apiToken,
    envConfig,
    instanceId
  );

  log("Fetching keys and data for instance...");
  const apiResp = await getMissingDataForInstance({
    apiToken,
    apiUrl: envConfig.api_url,
    instanceWithoutData,
  });

  log("Decrypting submission details...");
  const { instance, applicationDecryptor } = await decryptSharedData({
    apiToken,
    apiUrl: envConfig.api_url,
    instanceData: apiResp,
    maxcryptor,
    onCorruptedData: () => "🔒 Corrupted Data 🔒",
  });

  return {
    apiToken,
    instance,
    applicationDecryptor,
    encryptedInstance: apiResp,
  };
}

export async function processSubmission({
  applicationId,
  instanceId,
  verbose,
  config,
  _supliedConfig,
}: ProcessSubmissionInput): Promise<ProcessSubmissionOutput> {
  const log = verbose ? console.log : () => undefined;

  try {
    log("Fetching config...");
    const envConfig = await fetchEnvConfig(config.CLIENT_URL);

    const { instance, apiToken, applicationDecryptor, encryptedInstance } =
      _supliedConfig ||
      (await decryptStage(log, config, envConfig, instanceId));

    log("Checking verifications...");
    const [profileObjects, verifications] = splitPOAndVers(
      instance.profile_objects
    );

    const realVers =
      _supliedConfig?.realVers ||
      (await getSomeoneElsesRealVerifications({
        aId: instance.aa_id,
        apiUrl: envConfig.api_url,
        profileObjects,
        verifications,
      }));

    log("Fetching relevant schema...");

    const allSchemas = await getAllSchema(envConfig);

    log("Fetching access application...");
    const AA =
      instance.id === NULL_I_ID
        ? undefined
        : await fetchAA(apiToken, envConfig, instance.aa_id);

    let score: ScoreResult | undefined;
    if (AA && isScoreConfigValid(AA.ruleset)) {
      const ruleInputData = await convertInstanceToRuleInput(
        instance.profile_objects,
        realVers,
        async (schemaName) => {
          const schema = allSchemas.find((x) => x.name === schemaName);
          if (!schema) {
            throw new Error(`Could not find schema “${schemaName}”`);
          }
          return schema;
        }
      );

      try {
        // decrypt helper never uses a stored score. We always re-calculate it.
        log("Calculating score...");

        score = await calculateScore(AA.ruleset, ruleInputData);
        if (!equals(instance.score, score)) {
          // the score we calculated is different to the one that's saved on the instance.
          // So we save the new score.
          log("Saving score to instance...");

          // this can happen silently in the background, but we await it since so that we
          // abort if this fails.
          await updateInstanceData(apiToken, envConfig, encryptedInstance.id, {
            score,
          });
        }
      } catch (ex) {
        log(`Score calculation failed (${ex})`);
      }
    } else {
      log("No score rules configured on the AA.");
    }

    log("Fetching verification providers...");
    const PODetails = await formatOutput(
      profileObjects,
      allSchemas,
      realVers,
      apiToken,
      envConfig
    );

    log("Fetching attached files...");
    const files = await getFiles(
      profileObjects,
      instance,
      apiToken,
      envConfig,
      applicationDecryptor
    );

    const instanceDataToPassOn: InstanceDataToPassOn = omit(
      INSTANCE_FIELDS_TO_REMOVE,
      instance
    );

    // final outputs

    const csv = deepJsonToCsv({
      ...instanceDataToPassOn,
      ...mapObjIndexed(
        (POList) =>
          POList.map((flatPO) => {
            // this is a bit weird, but it prevents an even bigger breaking change for the csv format
            const object = Object.entries(flatPO.$properties).flatMap(
              ([fieldName, field]) => [
                [fieldName, field.value],
                [
                  `${fieldName}.verification`,
                  FIELD_VER_TEXT_MAP[field.verification],
                ],
              ]
            );
            return {
              ...flatPO,
              ...Object.fromEntries(object),
            };
          }),
        PODetails
      ),
    });

    log("Success!");

    return {
      json: {
        ...instanceDataToPassOn,
        score,
        profile_objects: PODetails,
      },
      csv,
      files,
      a_id: applicationId,
      client_url: config.CLIENT_URL,
      envConfig,
    };
  } catch (error) {
    log("Processing submission failed:", error);
    throw error; // pass on error
  }
}
