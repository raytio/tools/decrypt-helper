import { promises as fs } from "node:fs";
import { join } from "node:path";
import PdfPrinter from "pdfmake";
import type { TDocumentDefinitions } from "pdfmake/interfaces";
import JsxPdf from "jsx-pdf";
import { isValidLocale } from "../locales/index.js";
import { Report } from "../pdf/components/Report.js";
import { fonts } from "../pdf/style.js";
import { getDirname } from "../helpers/pathUtils.js";
import type { ProcessSubmissionOutput } from "./processSubmission.js";

export function generatePdfJson(
  data: ProcessSubmissionOutput,
  DATE_FORMAT: string,
  TIMEZONE: string
): TDocumentDefinitions {
  return JsxPdf.renderPdf(
    <Report
      data={data.json}
      files={data.files}
      aId={data.a_id}
      clientUrl={data.client_url}
      envConfig={data.envConfig}
      config={{ DATE_FORMAT, TIMEZONE }}
    />
  );
}

export const generatePDF =
  () =>
  // eslint-disable-next-line unicorn/consistent-function-scoping -- deliberately to future proof the SDK for options
  async (data: ProcessSubmissionOutput): Promise<ProcessSubmissionOutput> => {
    console.log("Generating PDF Report...");
    const {
      DATE_FORMAT = "en-nz",
      TIMEZONE = "Pacific/Auckland",
      PDF_LANGUAGE: customLang,
    } = process.env;

    if (data.envConfig.logo_url) {
      // there is a white labelling URL, so fetch it and write it to disk, overriding the default logo
      const arrayBuf = await fetch(data.envConfig.logo_url).then((r) =>
        r.arrayBuffer()
      );
      await fs.writeFile(
        join(getDirname(), "../../assets/custom-logo.png"),
        Buffer.from(arrayBuf)
      );
    }

    if (customLang) {
      if (isValidLocale(customLang)) {
        global.lang = customLang;
      } else {
        console.warn(
          `The specified PDF_LANGUAGE is language is invalid (${customLang})`
        );
      }
    }

    return new Promise((resolve) => {
      const printer = new PdfPrinter(fonts);

      const pdfDocument = printer.createPdfKitDocument(
        generatePdfJson(data, DATE_FORMAT, TIMEZONE)
      );

      const chunks: Uint8Array[] = [];

      pdfDocument.on("data", (chunk) => chunks.push(chunk));
      pdfDocument.on("end", () => {
        const final = Buffer.concat(chunks);
        const dataUrl = `data:application/pdf;base64,${final.toString(
          "base64"
        )}`;

        resolve({
          ...data,
          files: {
            ...data.files,
            // do not change name, will be a breaking change
            [data.json.id]: [dataUrl, "pdf"],
          },
        });
      });
      pdfDocument.end();
    });
  };
