import AWS from "aws-sdk";
import type { ProcessSubmissionOutput } from "./processSubmission.js";

type UploadOptions = {
  bucketName: string;
  fileName: string;
  fileContent: string | Buffer;
};

async function upload(
  s3: AWS.S3,
  { bucketName, fileName, fileContent }: UploadOptions
): Promise<AWS.S3.ManagedUpload.SendData> {
  return new Promise((resolve, reject) => {
    s3.upload(
      {
        Bucket: bucketName,
        Key: fileName,
        Body: fileContent,
      },
      (error, data) => {
        if (error) return reject(error);
        console.log(`Uploaded ${data.Location}`);
        return resolve(data);
      }
    );
  });
}

export const saveToS3Bucket =
  () =>
  // eslint-disable-next-line unicorn/consistent-function-scoping -- deliberately to future proof the SDK for options
  async (input: ProcessSubmissionOutput): Promise<void> => {
    console.log("Uploading data to S3...");
    const bucketName = process.env.S3_BUCKET;
    if (!bucketName) throw new Error("S3_BUCKET is not specified");

    const s3 = new AWS.S3();

    await upload(s3, {
      bucketName,
      fileName: `${input.json.id}/${input.json.id}.csv`,
      fileContent: input.csv,
    });

    await upload(s3, {
      bucketName,
      fileName: `${input.json.id}/${input.json.id}.json`,
      fileContent: JSON.stringify(input.json),
    });

    await Promise.all(
      Object.entries(input.files).map(
        async ([nId, [dataUrl, fileExtension]]) => {
          await upload(s3, {
            bucketName,
            fileName: `${input.json.id}/${nId}.${fileExtension}`,
            fileContent: Buffer.from(dataUrl.split(",")[1]!, "base64"),
          });
        }
      )
    );
    console.log("Finished uploading data to S3.");
  };
