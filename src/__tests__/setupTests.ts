/* eslint-disable import/no-extraneous-dependencies */
import { config as dotenv } from "dotenv";
import { configureToMatchImageSnapshot } from "jest-image-snapshot";

import "../configureEnv.js";
import type { Mock } from "vitest";

dotenv();

globalThis.m = (v: unknown): Mock => v as Mock;

const kebabCase = (text: string) =>
  text.replaceAll(/(\W+)/g, "-").replaceAll(/^-|-$/g, "").toLowerCase();

// Workaround for https://github.com/americanexpress/jest-image-snapshot/issues/345
const toMatchImageSnapshot = configureToMatchImageSnapshot({
  customSnapshotIdentifier(parameters) {
    const [filePath, describeTitle, testTitle] =
      parameters.currentTestName.split(" > ");
    const filename = filePath!.split("/").at(-1);
    const snapshotName = kebabCase(
      `${filename}-${describeTitle}-${testTitle}-${parameters.counter}-snap`
    );

    return snapshotName;
  },
});

declare module "vitest" {
  interface Assertion<T> {
    toMatchImageSnapshot(): T;
  }
}

expect.extend({ toMatchImageSnapshot });
