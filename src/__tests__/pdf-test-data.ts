import {
  type AId,
  FieldVerification,
  type IId,
  type KId,
  type NId,
  POVerification,
} from "@raytio/types";
import type { ProcessSubmissionOutput } from "../public-methods/processSubmission.js";

export default <ProcessSubmissionOutput["json"]>{
  id: <IId>"my_i_id",
  aa_id: <AId>"my_a_id",
  aack_id: <KId>"my_k_id",
  service_provider_n_id: <NId>"my_sp_n_id",
  start_date: "2020-06-06T12:34:56.789012",
  end_date: "2021-07-09T00:00:00",
  previous_state: "Received",
  data_provider_email: "user@example.com",
  state: "Received",
  _state: "Received",
  reference: "Ref-1234",
  confirmation_code: "123456",
  related_service_types: [],
  related_offers: [],
  thread: "",
  active: true,
  metadata: undefined,
  sub_service_provider_hash: "",
  score: {
    score: 14,
    category: "🟢 Automatically Accepted",
    categoryColour: "#1e9",
    diagnostics: 0 as never,
  },
  profile_objects: {
    ss_file: [
      {
        $nId: <NId>"n0",
        $schemaName: "ss_file",
        $schemaTitle: "A File",
        $badges: {
          verified: { code: POVerification.VerifiedFalse, statusText: "..." },
        },
        $verification_details: { date: "2022" },
        $shouldBeVerifiedFields: undefined,
        $properties: {
          title: {
            title: "Title",
            value: "My Signature",
            verification: FieldVerification.NotVerified,
          },
          content: {
            title: "Content",
            value: "urn:document:abc123",
            verification: FieldVerification.NotVerified,
          },
          tags: {
            title: "Tags",
            value: ["signature"],
            verification: FieldVerification.NotVerified,
          },
          lang: {
            title: "Content Language",
            value: "None",
            verification: FieldVerification.NotVerified,
          },
          directory: {
            title: "Directory",
            value: "Signatures",
            verification: FieldVerification.NotVerified,
          },
          contentLength: {
            title: "Content length",
            value: 3936,
            verification: FieldVerification.NotVerified,
          },
          date: {
            title: "Last modified date",
            value: "2020-09-06T22:41:21+00:00",
            verification: FieldVerification.NotVerified,
          },
          encoding: {
            title: "Content encoding",
            value: "None",
            verification: FieldVerification.NotVerified,
          },
          type: {
            title: "Content type",
            value: "image/png",
            verification: FieldVerification.NotVerified,
          },
        },
      },
    ],
    ss_person: [
      {
        $nId: <NId>"n1",
        $schemaName: "ss_Person_Name",
        $schemaTitle: "Person",
        $badges: {
          verified: { code: POVerification.FullyVerified, statusText: "..." },
        },
        $verification_details: { date: "2022" },
        $shouldBeVerifiedFields: undefined,

        $properties: {
          fName: {
            title: "First name",
            value: "John",
            verification: FieldVerification.NotVerified,
          },
          mName: {
            title: "Middle names",
            value: "🫒 🔒",
            verification: FieldVerification.NotVerified,
          },
          lName: {
            title: "Family name",
            value: "Doe",
            verification: FieldVerification.NotVerified,
          },
          consent: {
            title: "Marketing Consent",
            value: false,
            verification: FieldVerification.NotVerified,
          },
          other_guests: {
            title: "Other guests",
            value: [
              { "Family name": "Doe", "First name": "John" },
              { "First name": "Jane", "Family name": "Doe" },
              {
                "First name": "Jane",
                "Family name": "Doe",
                "Middle name": "Joyce",
              },
            ],
            verification: FieldVerification.NotVerified,
          },
        },
      },
    ],

    // this is a group name
    identity_document: [
      {
        $nId: <NId>"n2",
        $schemaName: "ss_NZ_DL",
        $schemaTitle: "NZ Driver Licence",
        $badges: {
          verified: {
            code: POVerification.PartiallyVerified,
            statusText: "...",
          },
        },
        $verification_details: { date: "2022", verifier_id: "Waka Kotahi" },
        $shouldBeVerifiedFields: [],

        $properties: {
          consent: {
            title: "Consent",
            value: true,
            verification: FieldVerification.NotVerified,
          },
          id: {
            title: "Driver licence number",
            value: "DW0123456",
            verification: FieldVerification.Verified,
          },
          version: {
            title: "Driver licence version",
            value: 513,
            verification: FieldVerification.NotVerified,
          },
          fName: {
            title: "First name",
            value: "John",
            verification: FieldVerification.NotVerified,
          },
          mName: {
            title: "Middle names",
            value: "Steve",
            verification: FieldVerification.NotVerified,
          },
          lName: {
            title: "Family name",
            value: "Doe",
            verification: FieldVerification.NotVerified,
          },
          DoB: {
            title: "Day of birth",
            value: 29,
            verification: FieldVerification.VerifiedFalse,
          },
          MoB: {
            title: "Month of birth",
            value: 2,
            verification: FieldVerification.NotVerified,
          },
          YoB: {
            title: "Year of birth",
            value: 2002,
            verification: FieldVerification.Verified,
          },
          imageFront: {
            title: "Driver Licence front image",
            value: {
              content: "urn:document:abc456",
              last_modified: "2020-09-06T22:27:38+00:00",
              content_language: "None",
              tags: [
                "ss_NZ_DriverLicence",
                "identity_document_picture_extracted_photo_page",
              ],
              content_length: 704311,
              title: "Driver Licence front image",
              content_type: "image/jpeg",
              directory: "_referenced/Driver Licence",
              content_encoding: "None",
              n_id: "abc456",
            },
            verification: FieldVerification.Verified,
          },
          imageSig: {
            title: "Extracted signature image",
            value: {
              content_language: "None",
              last_modified: "2020-09-06T22:27:37+00:00",
              tags: [
                "ss_NZ_DriverLicence",
                "identity_document_picture_extracted_signature",
              ],
              content: "urn:document:abc789",
              title: "Extracted signature image",
              content_type: "image/jpeg",
              content_encoding: "None",
              content_length: 40567,
              directory: "_referenced/Driver Licence",
              n_id: "abc789",
            },
            verification: FieldVerification.NotVerified,
          },
          imagePerson: {
            title: "Extracted person image",
            value: {
              content_encoding: "None",
              content_type: "image/jpeg",
              content_language: "None",
              title: "Extracted person image",
              content: "urn:document:abc012",
              tags: [
                "ss_NZ_DriverLicence",
                "identity_document_picture_extracted_person",
              ],
              content_length: 79651,
              directory: "_referenced/Driver Licence",
              last_modified: "2020-09-06T22:27:37+00:00",
              n_id: "abc012",
            },
            verification: FieldVerification.NotVerified,
          },
          validation: {
            title: "Validation",
            value: {
              score: 0.4,
              breakdown: {
                a: {
                  severity: "medium",
                  passed: false,
                  reason: "Āā",
                  code: 110,
                },
                b: { passed: true },
                c: {
                  severity: "high",
                  passed: false,
                  reason: "Oh no",
                  code: 120,
                },
                d: { passed: true },
                e: { severity: "low", passed: false, reason: "Ēē", code: 130 },
              },
              warning: ["A", "B", "C"],
            },
            verification: FieldVerification.NotVerified,
          },
        },
      },
    ],
    contanct_location: [
      {
        $nId: <NId>"n3",
        $schemaName: "ss_NZ_DL",
        $schemaTitle: "Location",
        $badges: {
          verified: { code: POVerification.NotVerified, statusText: "..." },
        },
        $verification_details: { date: "2022" },
        $shouldBeVerifiedFields: undefined,
        $properties: {
          object: {
            title: "object",
            value: { a: 1, b: "Two" },
            verification: FieldVerification.NotVerified,
          },
          array: {
            title: "array",
            value: ["One", 2],
            verification: FieldVerification.NotVerified,
          },
          nil: {
            title: "nil",
            value: null,
            verification: FieldVerification.NotVerified,
          },
          houseNumber: {
            title: "Street Number",
            value: "224",
            verification: FieldVerification.NotVerified,
          },
          street: {
            title: "Street or route",
            value: "Albany Hwy",
            verification: FieldVerification.NotVerified,
          },
          suburb: {
            title: "Suburb",
            value: "Schnapper Rock",
            verification: FieldVerification.NotVerified,
          },
          city: {
            title: "City",
            value: "Auckland",
            verification: FieldVerification.NotVerified,
          },
          state: {
            title: "State",
            value: "AKL",
            formatted_value: "Auckland",
            verification: FieldVerification.NotVerified,
          },
          postcode: {
            title: "Post code",
            value: "0632",
            verification: FieldVerification.NotVerified,
          },
          country: {
            title: "Location country",
            value: "NZ",
            verification: FieldVerification.NotVerified,
          },
        },
      },
    ],
    ss_multiple_test: [
      {
        $nId: <NId>"n4",
        $schemaName: "ss_multiple_test",
        $schemaTitle: "Multiple of the same Schema",
        $badges: {
          verified: { code: POVerification.NotVerified, statusText: "..." },
        },
        $verification_details: { date: "2022" },
        $shouldBeVerifiedFields: undefined,
        $properties: {
          mName: {
            title: "Middle Name",
            value: "Sørensen",
            verification: FieldVerification.VerifiedFalse,
          },
          lName: {
            title: "Last Name",
            value: "Sterling-Shepard",
            verification: FieldVerification.Verified,
          },
        },
      },
      {
        $nId: <NId>"n5",
        $schemaName: "ss_multiple_test",
        $schemaTitle: "Multiple of the same Schema",
        $badges: {
          verified: { code: POVerification.NotVerified, statusText: "..." },
        },
        $verification_details: { date: "2022" },
        $shouldBeVerifiedFields: undefined,
        $properties: {
          fName: {
            title: "First Name",
            value: "Bjørn",
            verification: FieldVerification.NotVerified,
          },
          lName: {
            title: "Last Name",
            value: "Shepherd",
            verification: FieldVerification.VerifiedFalse,
          },
        },
      },
      {
        $nId: <NId>"n6",
        $schemaName: "ss_multiple_test",
        $schemaTitle: "Multiple of the same Schema",
        $badges: {
          verified: { code: POVerification.NotVerified, statusText: "..." },
        },
        $verification_details: { date: "2022" },
        $shouldBeVerifiedFields: undefined,
        $properties: {
          fName: {
            title: "First Name",
            value: "Bjørn",
            verification: FieldVerification.Verified,
          },
          mName: {
            title: "Middle Name",
            value: "Sørensen",
            verification: FieldVerification.NotVerified,
          },
        },
      },
    ],
  },
};
