import { pdf } from "pdf-to-img";
import type { AId } from "@raytio/types";
import { generatePDF, generatePdfJson } from "../public-methods/generatePDF";
import type { ProcessSubmissionOutput } from "../public-methods/processSubmission";
import type { EnvConfig } from "../api";
import json from "./pdf-test-data";

vi.mock("../public-methods/version", () => ({
  version: "💖",
}));

const pdfArguments: ProcessSubmissionOutput = {
  a_id: <AId>"my a_id",
  csv: "",
  client_url: "https://example.com",
  json,
  files: {},
  envConfig: { app_name: "Raytio" } as EnvConfig,
};

describe("pdf", () => {
  it("generates valid PDF json from sample data", () => {
    global.lang = "en";
    const genPdf = generatePdfJson(
      pdfArguments,
      "de-DE",
      "Arctic/Longyearbyen"
    );

    delete genPdf.ownerPassword; // because it's random
    delete genPdf.images; // since contains full path on disk

    expect(genPdf).toMatchSnapshot();
  });

  it("generates a valid PDF from sample data", async () => {
    process.env.TIMEZONE = "Arctic/Longyearbyen";
    process.env.DATE_FORMAT = "de-DE";
    const submissionData = await generatePDF()(pdfArguments);

    const files = Object.values(submissionData.files);
    expect(files).toHaveLength(1); // 1 pdf

    const [dataUrl] = files[0]!;

    const document = await pdf(dataUrl);

    expect(document).toHaveLength(2);
    for await (const page of document) {
      expect(page).toMatchImageSnapshot();
    }
  });

  it("can whitelabel the pdf", async () => {
    // this test makes an actual API call to fecth the logo
    process.env.TIMEZONE = "Asia/Ulaanbaatar";
    process.env.DATE_FORMAT = "lb-LU";

    const updatedPdfArguments = {
      ...pdfArguments,
      envConfig: {
        app_name: "Example Company",
        logo_url:
          "https://upload.wikimedia.org/wikipedia/commons/7/70/Example.png",
      } as EnvConfig,
    };
    updatedPdfArguments.json.profile_objects = {}; // keep this test simple

    const submissionData = await generatePDF()(updatedPdfArguments);

    const files = Object.values(submissionData.files);
    expect(files).toHaveLength(1); // 1 pdf

    const [dataUrl] = files[0]!;

    const document = await pdf(dataUrl);

    expect(document).toHaveLength(1);
    for await (const page of document) {
      expect(page).toMatchImageSnapshot();
    }
  });
});
