import type { AId } from "@raytio/types";
import AWS from "aws-sdk";
import { saveToS3Bucket } from "..";
import type { EnvConfig } from "../api";
import json from "./pdf-test-data";

vi.mock("aws-sdk");

describe("saveToS3Bucket", () => {
  beforeEach(() => {
    process.env.S3_BUCKET = "my_bucket";
  });

  it("calls the AWS API correctly", async () => {
    const upload = vi.fn((_, callback) =>
      callback(null, { Location: "~/folder" })
    );
    m(AWS.S3).mockImplementation(() => ({
      upload,
    }));
    const output = await saveToS3Bucket()({
      json,
      csv: "a,b\n1,2",
      files: {
        report_123: ["data:text/html,<p>Hi</p>", "html"],
        n_id_of_file: ["data:text/plain;base64,SGk", "txt"],
      },
      a_id: "my_a_id" as AId,
      client_url: "https://example.org",
      envConfig: {} as EnvConfig,
    });

    expect(output).toBeUndefined();
    expect(upload).toHaveBeenCalledTimes(4);
    expect(upload).toHaveBeenNthCalledWith(
      1,
      {
        Body: "a,b\n1,2",
        Bucket: "my_bucket",
        Key: "my_i_id/my_i_id.csv",
      },
      expect.any(Function)
    );
    expect(upload).toHaveBeenNthCalledWith(
      2,
      {
        Body: JSON.stringify(json),
        Bucket: "my_bucket",
        Key: "my_i_id/my_i_id.json",
      },
      expect.any(Function)
    );
    expect(upload).toHaveBeenNthCalledWith(
      3,
      {
        Body: Buffer.from("<p>Hi</p>", "base64"),
        Bucket: "my_bucket",
        Key: "my_i_id/report_123.html",
      },
      expect.any(Function)
    );
    expect(upload).toHaveBeenNthCalledWith(
      4,
      {
        Body: Buffer.from("SGk", "base64"),
        Bucket: "my_bucket",
        Key: "my_i_id/n_id_of_file.txt",
      },
      expect.any(Function)
    );
  });
});
