import { createHmac } from "node:crypto";
import { promises as fs } from "node:fs";
import { join } from "node:path";
import { pdf } from "pdf-to-img";
import type { AId, IId } from "@raytio/types";
import { generatePDF, getAndValidateConfig, processSubmission } from "..";

const hash = (string: string) =>
  createHmac("sha256", ".").update(string).digest("hex");

describe("integration tests", () => {
  beforeAll(async () => {
    await fs.mkdir(join(__dirname, "./__manual_snapshots__"), {
      recursive: true,
    });
  });

  beforeEach(() => {
    process.env.S3_BUCKET = "hi";
    process.env.DATE_FORMAT = "en-nz";
    process.env.TIMEZONE = "Pacific/Auckland";
    process.env.CLIENT_URL = "https://app-dev.rayt.io";
    process.env.API_DOCS_URL = "https://api-docs.rayt.io/";
  });

  it("calls the correct APIs", async () => {
    const REQUIRED_ENV_VARs = [
      "RAYTIO_USERNAME",
      "RAYTIO_PASSWORD",
      "A_ID",
      "I_ID_1",
    ];

    if (REQUIRED_ENV_VARs.some((v) => !process.env[v])) {
      throw new Error("Not all ENV variables are set");
    }

    const submissionData = await processSubmission({
      instanceId: process.env.I_ID_1! as IId,
      applicationId: process.env.A_ID! as AId,
      config: getAndValidateConfig(),
      verbose: true,
    });

    expect(submissionData).toMatchSnapshot();

    await fs.writeFile(
      join(__dirname, "./__manual_snapshots__/1.csv"),
      submissionData.csv
    );
  });

  it("generates a PDF for the reproduction example from decrypt-helper#18", async () => {
    const REQUIRED_ENV_VARs = [
      "RAYTIO_USERNAME",
      "RAYTIO_PASSWORD",
      "A_ID",
      "I_ID_2",
    ];

    if (REQUIRED_ENV_VARs.some((v) => !process.env[v])) {
      throw new Error("Not all ENV variables are set");
    }

    const submissionData = await processSubmission({
      instanceId: process.env.I_ID_2! as IId,
      applicationId: process.env.A_ID! as AId,
      config: getAndValidateConfig(),
      verbose: true,
    }).then(generatePDF());

    const files = Object.values(submissionData.files);
    expect(files).toHaveLength(1); // 1 pdf. The three encrypted, corrupted images are skipped

    const [dataUrl] = files[0]!;

    const document = await pdf(dataUrl);

    expect(document).toHaveLength(1);
    expect(document.metadata).toStrictEqual({
      CreationDate: expect.any(String),
      Creator: "Raytio",
      IsAcroFormPresent: false,
      IsCollectionPresent: false,
      IsLinearized: false,
      IsSignaturesPresent: false,
      IsXFAPresent: false,
      PDFFormatVersion: "1.3",
      Producer: "Raytio",
      Title: `Raytio Verification Report - ${process.env.I_ID_2}`,
      Subject: expect.any(String),
      EncryptFilterName: "Standard",
      Language: null,
    });

    for await (const page of document) {
      expect(page).toMatchImageSnapshot();
    }
  });

  it("does not include base64 in the JSON file (repro for decrypt-helper#31)", async () => {
    const REQUIRED_ENV_VARs = [
      "RAYTIO_USERNAME",
      "RAYTIO_PASSWORD",
      "A_ID",
      "I_ID_4",
    ];

    if (REQUIRED_ENV_VARs.some((v) => !process.env[v])) {
      throw new Error("Not all ENV variables are set");
    }

    const submissionData = await processSubmission({
      instanceId: process.env.I_ID_4! as IId,
      applicationId: process.env.A_ID! as AId,
      config: getAndValidateConfig(),
      verbose: true,
    });

    submissionData.json.previous_state = "Received";
    submissionData.json.state = "Received";
    // submissionData.json._state = "Received";

    expect(submissionData.json).toMatchSnapshot();

    for (const file in submissionData.files) {
      const [dataUrl, fileExtension] = submissionData.files[file]!;

      // jest-image-snapshot doesn't support jpeg so we save the file to see that it's valid. as well as snapshotting the hash
      await fs.writeFile(
        join(__dirname, `./__manual_snapshots__/${file}.${fileExtension}`),
        dataUrl.split(",")[1]!,
        "base64"
      );
      expect(hash(dataUrl)).toMatchSnapshot();
    }
  });

  it("works for encrypted images", async () => {
    const REQUIRED_ENV_VARs = [
      "RAYTIO_USERNAME",
      "RAYTIO_PASSWORD",
      "A_ID",
      "I_ID_3",
    ];

    if (REQUIRED_ENV_VARs.some((v) => !process.env[v])) {
      throw new Error("Not all ENV variables are set");
    }

    const submissionData = await processSubmission({
      instanceId: process.env.I_ID_3! as IId,
      applicationId: process.env.A_ID! as AId,
      config: getAndValidateConfig(),
      verbose: true,
    }).then(generatePDF());

    const files = Object.values(submissionData.files);
    expect(files).toHaveLength(4); // 1 pdf + 3 images in DL

    const [pdfDataUrl] = files.find((f) => f[1] === "pdf")!;

    for await (const page of await pdf(pdfDataUrl)) {
      expect(page).toMatchImageSnapshot();
    }
  });

  it("can verify submissions that use hashed_n_id (repro for decrypt-helper#36)", async () => {
    const REQUIRED_ENV_VARs = [
      "RAYTIO_USERNAME",
      "RAYTIO_PASSWORD",
      "A_ID",
      "I_ID_5",
    ];

    if (REQUIRED_ENV_VARs.some((v) => !process.env[v])) {
      throw new Error("Not all ENV variables are set");
    }

    const submissionData = await processSubmission({
      instanceId: process.env.I_ID_5! as IId,
      applicationId: process.env.A_ID! as AId,
      config: getAndValidateConfig(),
      verbose: true,
    });

    submissionData.json.previous_state = "Received";
    submissionData.json.state = "Received";
    // submissionData.json._state = "Received";

    expect(submissionData.json).toMatchSnapshot();
  });
});
