import { repairDate } from "@raytio/core";
import { $$ } from "../../locales/index.js";
import type { PdfConfig } from "../types";
import { formatDate } from "./general.js";

export const IS_IMAGE = Symbol.for("image");
export const IS_ARRAY_OF_OBJECTS = Symbol.for("ArrayOfObjects");

export function transform(
  key: string,
  value: unknown,
  pdfConfig: PdfConfig,
  imageFieldNames?: string[]
): string | typeof IS_IMAGE | typeof IS_ARRAY_OF_OBJECTS {
  // value is possible to be false if type boolean
  if (value === undefined) {
    return $$("field.undefined");
  }
  if (typeof value === "boolean") {
    return value ? $$("field.boolean.yes") : $$("field.boolean.no");
  }
  if (imageFieldNames?.includes(key)) {
    return IS_IMAGE;
  }
  if (Array.isArray(value)) {
    if (value.every((v) => typeof v === "object")) {
      return IS_ARRAY_OF_OBJECTS;
    }
    return value.join(", ");
  }

  if (typeof value !== "string") {
    return `${value}`;
  }
  switch (key) {
    case "start_date":
    case "end_date": {
      return formatDate(
        repairDate(value),
        pdfConfig.DATE_FORMAT,
        pdfConfig.TIMEZONE
      );
    }
    default: {
      return value;
    }
  }
}
