import { readFileSync } from "node:fs";
import { join } from "node:path";
import { FieldVerification, POVerification } from "@raytio/types";
import { getDirname } from "../../helpers/pathUtils.js";

export const asset = (name: string): string => {
  if (/[^.A-Z_a-z-]/.test(name) || name.includes("..")) {
    throw new Error("Invalid asset name");
  }

  return join(getDirname(), "./../../assets/", name);
};

export const loadAsset = (name: string): string =>
  readFileSync(asset(name), { encoding: "utf8" });

export const verifyColour = (x: FieldVerification | POVerification): string => {
  if (x === FieldVerification.Verified) return "#12130e";

  if (
    x === FieldVerification.VerifiedFalse ||
    x === POVerification.VerifiedFalse
  ) {
    return "#fb7171";
  }

  return "#000";
};

/**
 * Produces a date with a long month
 */
export const formatDate = (d: Date, locale: string, timeZone: string): string =>
  new Intl.DateTimeFormat(locale, {
    day: "2-digit",
    month: "long",
    year: "numeric",
    hour: "numeric",
    minute: "numeric",
    timeZone,
  }).format(d);
