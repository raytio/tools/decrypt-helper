import type { IId, Instance } from "@raytio/types";
import { $$ } from "../locales/index.js";

export const NULL_I_ID = <IId>"00000000-0000-0000-0000-000000000000";

export const SUBMISSION_DATA: Partial<Record<keyof Instance, string>> = {
  confirmation_code: $$("constants.confirmation_code"),
  id: $$("constants.i_id"),
  reference: $$("constants.reference"),
  start_date: $$("constants.start_date"),
  end_date: $$("constants.end_date"),
};
