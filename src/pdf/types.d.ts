export type PdfConfig = {
  DATE_FORMAT: string;
  TIMEZONE: string;
};
