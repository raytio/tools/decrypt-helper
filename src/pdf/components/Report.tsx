import { randomBytes } from "node:crypto";
import JsxPdf from "jsx-pdf";
import type { AId } from "@raytio/types";
import { isScoreResultValid } from "@raytio/core";
import { $$ } from "../../locales/index.js";
import type { PdfConfig } from "../types.js";
import { classes } from "../style.js";
import { asset } from "../helpers/general.js";
import { NULL_I_ID, SUBMISSION_DATA } from "../constants.js";
import { transform } from "../helpers/transform.js";
import type { EnvConfig } from "../../api/index.js";
import { type InstanceDataToPassOn, SCHEMA } from "../../constants.js";
import type { ProcessSubmissionOutput } from "../../public-methods/processSubmission.js";
import { version } from "../../public-methods/version.js";
import { VerifyBox } from "./VerifyBox.js";
import { Table } from "./Table.js";
import { TableTitle } from "./TableTitle.js";
import { Subheader } from "./Subheader.js";
import { POVerificationBadge } from "./POVerificationBadge.js";

export const Report = ({
  data,
  files,
  config,
  aId,
  clientUrl,
  envConfig,
}: {
  data: ProcessSubmissionOutput["json"];
  files: ProcessSubmissionOutput["files"];
  config: PdfConfig;
  aId: AId;
  clientUrl: string;
  envConfig: EnvConfig;
}): JSX.Element => {
  // The PDF is read only; there's no reason why anyone would ever need to unlock it.
  const randomToken = randomBytes(32).toString("base64");

  const schemas = Object.values(data.profile_objects).sort(
    // sort it so that the person schema comes first
    (a, b) =>
      +(b[0]?.$schemaName === SCHEMA.PERSON) -
      +(a[0]?.$schemaName === SCHEMA.PERSON)
  );

  const appName = envConfig.app_name;
  const iId = data.id === NULL_I_ID ? "" : data.id;

  return (
    <document
      defaultStyle={{ font: "Lato", fontSize: 12 }}
      info={{
        title: $$("Report.meta.title", { appName, iId }),
        creator: appName,
        producer: "Raytio", // don't whitelabel
        subject: version,
      }}
      // @ts-expect-error -- waiting on bpampuch/pdfmake#2453
      lang={global.lang}
      ownerPassword={randomToken}
      permissions={{
        printing: "highResolution",
        copying: true, // allow copy/paste
      }}
      pageSize="A4"
      styles={classes}
      images={{
        raytioLogo: envConfig.logo_url
          ? asset("custom-logo.png")
          : asset("logo.png"),
      }}
    >
      <header>
        {(currentPage) =>
          currentPage !== 1 && (
            <stack color="#333" fontSize={10}>
              <text margin={16}>
                {/* <image src="raytioLogo" /> */}
                <text bold>{$$("Report.header", { appName })} - </text>
                <text>{iId}</text>
              </text>
            </stack>
          )
        }
      </header>

      <content>
        <image src="raytioLogo" width={150} alignment="center" />
        <text style={["header", "marginBottom"]} alignment="center">
          {$$("Report.header", { appName })}
        </text>
        <columns columnGap={10}>
          <column width="auto">
            <text style={["subheader", "marginBottom"]}>
              {/* convert the metadata form the instance into a key-value pair list */}
              {iId &&
                Object.entries(SUBMISSION_DATA)
                  .map(
                    ([id, label]) =>
                      `${label}: ${
                        transform(
                          id,
                          data[id as keyof InstanceDataToPassOn],
                          config
                        ) as string
                      }`
                  )
                  .join("\n")}
              {isScoreResultValid(data.score)
                ? [
                    "",
                    `${$$("constants.score")}: ${data.score.score}`,
                    `${$$("constants.score-category")}: ${data.score.category}`,
                  ].join("\n")
                : ""}
            </text>
            <Subheader data={data} config={config} />
          </column>

          {/* this expands to as wide as possible to push the other columns all the way left and right */}
          <column width="*">
            <text> </text>
          </column>

          <column width={150}>
            <VerifyBox aId={aId} iId={data.id} clientUrl={clientUrl} />
          </column>
        </columns>

        {schemas.map((POs) => {
          // for each schema
          return (
            <stack
              // unbreakable is off because it crashes if its too big for one page
              // This is a 7-year-old issue in pdfmake (https://github.com/bpampuch/pdfmake/issues/207)
              unbreakable={false}
              style="marginY"
            >
              <TableTitle POs={POs} />

              <columns columnGap={15}>
                <column width={80}>
                  <POVerificationBadge width={80} POs={POs} />
                </column>
                <column width="auto">
                  <Table POs={POs} files={files} config={config} />
                </column>
              </columns>
            </stack>
          );
        })}
      </content>

      <footer>
        {(currentPage, pageCount, pageSize) => (
          <stack bold color="#333" fontSize={10}>
            <text margin={16}>
              <text>{$$("Report.header", { appName })} - </text>
              <text color="#fb7171">{$$("Report.footer-private")}</text>
            </text>
            <text absolutePosition={{ x: pageSize.width - 50, y: 15 }}>
              {$$("Report.pagecount", { currentPage, pageCount })}
            </text>
          </stack>
        )}
      </footer>
    </document>
  );
};
