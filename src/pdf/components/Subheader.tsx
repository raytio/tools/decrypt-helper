import { FieldVerification } from "@raytio/types";
import JsxPdf from "jsx-pdf";
import { $$ } from "../../locales/index.js";
import { FieldVerificationBadge } from "./FieldVerificationBadge.js";

export const Subheader = (): JSX.Element => {
  return (
    <>
      <text bold style="marginBottom">
        Key:
      </text>
      <table layout="noBorders">
        <row>
          <cell>
            <FieldVerificationBadge
              width={10}
              status={FieldVerification.Verified}
            />
          </cell>
          <cell>{$$("FieldVerification.Verified")}</cell>
        </row>
        <row>
          <cell>
            <FieldVerificationBadge
              width={10}
              status={FieldVerification.NotVerified}
            />
          </cell>
          <cell>{$$("FieldVerification.NotVerified")}</cell>
        </row>
        <row>
          <cell>
            <FieldVerificationBadge
              width={10}
              status={FieldVerification.VerifiedFalse}
            />
          </cell>
          <cell>{$$("FieldVerification.VerifiedFalse")}</cell>
        </row>
      </table>
    </>
  );
};
