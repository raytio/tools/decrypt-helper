import { POVerification } from "@raytio/types";
import JsxPdf from "jsx-pdf";
import { $$ } from "../../locales/index.js";
import { loadAsset } from "../helpers/general.js";
import type { FlatPO } from "../../helpers/index.js";
import { PO_VER_TEXT_MAP } from "../../constants.js";

export const FILE_MAP: Record<POVerification, string> = {
  [POVerification.FullyVerified]: "verified_user_big",
  [POVerification.PartiallyVerified]: "verified_partial_big",
  [POVerification.NotVerified]: "not_verified_big",
  [POVerification.Expired]: "not_verified_big",
  [POVerification.VerifiedFalse]: "false_verified_user_big",

  // not possible but included here for completeness
  [POVerification.Encrypted]: "not_verified_big",
  [POVerification.Loading]: "not_verified_big",
};

export const POVerificationBadge = ({
  width,
  POs,
}: {
  width: number;
  POs: FlatPO[];
}): JSX.Element => {
  // TODO: this won't work if multiple POs of the same schema are shared
  const { $badges, $verification_details, $shouldBeVerifiedFields } = POs[0]!;
  const verifiedBy = $verification_details?.verifier_id;

  return (
    <>
      {$shouldBeVerifiedFields && (
        <>
          <svg
            content={loadAsset(`${FILE_MAP[$badges.verified.code]}.svg`)}
            width={width}
          />
          <text alignment="center">
            {PO_VER_TEXT_MAP[$badges.verified.code]}
          </text>
          {verifiedBy && (
            <text alignment="center">
              {$$("POVerificationBadge.verified-by", { verifiedBy })}
            </text>
          )}
        </>
      )}

      {$badges.safeHarbour && (
        <columns columnGap={10}>
          <column width={width / 3}>
            <svg
              content={loadAsset(
                `${
                  FILE_MAP[
                    $badges.safeHarbour.code
                      ? POVerification.FullyVerified
                      : POVerification.VerifiedFalse
                  ]
                }.svg`
              )}
              width={width / 3}
            />
          </column>
          <column width="auto">
            <text alignment="center">
              {$badges.safeHarbour.code
                ? $$("POVerificationBadge.safe-harbour-yes")
                : $$("POVerificationBadge.safe-harbour-no")}
            </text>
          </column>
        </columns>
      )}
    </>
  );
};
