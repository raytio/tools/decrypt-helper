import JsxPdf from "jsx-pdf";
import { type FlatPO, isPOFile } from "../../helpers/index.js";

export const TableTitle = ({ POs }: { POs: FlatPO[] }): JSX.Element => {
  const schemaTitle = POs[0]!.$schemaTitle;

  const maybeFileName = POs.some(isPOFile)
    ? (POs[0]!.$properties.title?.value as string | undefined)
    : undefined;

  return (
    <text bold fontSize={14} style="marginBottom">
      {maybeFileName || schemaTitle}
    </text>
  );
};
