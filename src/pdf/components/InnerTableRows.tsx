import JsxPdf from "jsx-pdf";
import { uniq } from "ramda";
import { assertSafeProperty } from "../../helpers/index.js";
import type { ProcessSubmissionOutput } from "../../public-methods/processSubmission.js";
import {
  IS_ARRAY_OF_OBJECTS,
  IS_IMAGE,
  transform,
} from "../helpers/transform.js";
import type { PdfConfig } from "../types.js";
import { Images } from "./Images.js";
import { ValidationDisplay } from "./ValidationDisplay.js";

export const InnerTableRows = ({
  key,
  value,
  imageFieldNames,
  config,
  files,
  color,
}: {
  key: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  value: any;
  files: ProcessSubmissionOutput["files"];
  config: PdfConfig;
  color: string;
  imageFieldNames: string[];
}): JSX.Element => {
  if (
    typeof value === "object" &&
    value &&
    "score" in value &&
    "breakdown" in value
  ) {
    return <ValidationDisplay value={value} />;
  }
  const text = transform(key, value, config, imageFieldNames);

  if (typeof text !== "string" && text === IS_IMAGE) {
    return <Images files={files} nIds={[value.n_id]} />;
  }

  if (typeof text !== "string" && text === IS_ARRAY_OF_OBJECTS) {
    const headers: string[][] = [uniq(value.flatMap(Object.keys))];
    const tableValues = [...headers, ...value];

    return (
      <table headerRows={1} layout="headerLineOnly">
        {tableValues.map((data, tableValueIndex) => {
          return (
            <row>
              {headers[0]!.map((header) => {
                assertSafeProperty(header);
                return (
                  <cell>
                    <InnerTableRows
                      key={header}
                      value={
                        data[header] || (tableValueIndex === 0 ? header : "-")
                      }
                      files={files}
                      config={config}
                      color={color}
                      imageFieldNames={imageFieldNames}
                    />
                  </cell>
                );
              })}
            </row>
          );
        })}
      </table>
    );
  }

  return <text color={color}>{text.slice(0, 100)}</text>;
};
