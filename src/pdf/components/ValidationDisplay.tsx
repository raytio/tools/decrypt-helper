import JsxPdf from "jsx-pdf";
import type { Validation } from "@raytio/types";
import { $$ } from "../../locales/index.js";
import { assertSafeProperty } from "../../helpers/index.js";
import { pieChart } from "./pieChart.js";

type Severity = NonNullable<Validation["breakdown"][string]["severity"]>;

const SEVERITY_COLOURS: Record<Severity, string> = {
  low: "#2e8bc0",
  medium: "#ff9800",
  high: "#fb7171",
};

export const ValidationDisplay = ({
  value,
}: {
  value: Validation;
}): null | JSX.Element => {
  const colour = value.score > 0.5 ? "#c2d887" : "#fb7171";
  return (
    <>
      <svg content={pieChart(colour, value.score * 100)} width={50} />
      {$$("ValidationDisplay.text", { n: value.score * 10 })}
      {Object.values(value.breakdown)
        .filter((x) => x.severity && x.reason)
        .map((x) => {
          const key = x.severity!;
          assertSafeProperty(key);
          return <text color={SEVERITY_COLOURS[key]}>{x.reason!}</text>;
        })}
    </>
  );
};
