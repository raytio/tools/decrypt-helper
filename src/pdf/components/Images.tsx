import JsxPdf from "jsx-pdf";
import { $$ } from "../../locales/index.js";
import { assertSafeProperty } from "../../helpers/index.js";
import type { ProcessSubmissionOutput } from "../../public-methods/processSubmission.js";

export const Images = ({
  nIds,
  files,
}: {
  nIds: string[];
  files: ProcessSubmissionOutput["files"];
}): JSX.Element => {
  return (
    <>
      {nIds.map((nId) => {
        // for videos, prefer the static frame over the original video
        const file = (files[`${nId}_videoFrame`] || files[nId])?.[0];

        if (file) assertSafeProperty(file);
        return file ? (
          <image src={file} width={300} />
        ) : (
          <text>{$$("Images.file-not-found")}</text>
        );
      })}
    </>
  );
};
