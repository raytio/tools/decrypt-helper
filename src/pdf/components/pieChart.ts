/**
 * A semi-cirle pie chart
 * (!) generates a SVG string, not a JSX component.
 * @param percentage a number from 0 to 100
 */
export function pieChart(colour: string, percentage: number): string {
  return `
    <svg height="70" width="140" viewBox="0 -5 20 15">
      <circle r="10" cx="10" cy="10" fill="bisque" />
      <circle
        r="5"
        cx="10"
        cy="10"
        fill="white"
        stroke="${colour}"
        stroke-width="10"
        stroke-dasharray="${(((percentage / 2) * 31.42) / 100).toFixed(
          2 // NOTE: css calc() doesn't work in PDFs
        )} 31.42"
        transform="rotate(-180) translate(-20, -20)"
      />
    </svg>
`;
}
