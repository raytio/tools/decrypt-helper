import JsxPdf from "jsx-pdf";
import { FieldVerification } from "@raytio/types";
import { loadAsset } from "../helpers/general.js";

export const FieldVerificationBadge = ({
  status,
  width,
}: {
  status: FieldVerification | undefined;
  width: number;
}): null | JSX.Element => {
  if (status === FieldVerification.VerifiedFalse) {
    return (
      <svg content={loadAsset("verified_false_small.svg")} width={width} />
    );
  }

  if (status === FieldVerification.Verified) {
    return <svg content={loadAsset("verified_tick_small.svg")} width={width} />;
  }

  return null;
};
