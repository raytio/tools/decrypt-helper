import JsxPdf from "jsx-pdf";
import { verifyColour } from "../helpers/general.js";
import type { PdfConfig } from "../types.js";
import {
  type FlatPO,
  assertSafeProperty,
  isFieldValueFile,
  isPOFile,
} from "../../helpers/index.js";
import type { ProcessSubmissionOutput } from "../../public-methods/processSubmission.js";
import { FieldVerificationBadge } from "./FieldVerificationBadge.js";
import { Images } from "./Images.js";
import { InnerTableRows } from "./InnerTableRows.js";

export const Table = ({
  POs,
  files,
  config,
}: {
  POs: FlatPO[];
  files: ProcessSubmissionOutput["files"];
  config: PdfConfig;
}): JSX.Element => {
  /** every fieldName in these profile object */
  const fieldNames = [
    ...new Set(POs.flatMap((PO) => Object.keys(PO.$properties))),
  ];

  // some or all POs in this schema are files themselves. So render the file, assuming it's an image
  const fileNIds = POs.filter(isPOFile).map((x) => x.$nId);

  if (fileNIds.length) return <Images nIds={fileNIds} files={files} />;

  const imageFieldNames = fieldNames.filter((fieldName) =>
    POs.map((PO) => PO.$properties[fieldName]?.value).some(isFieldValueFile)
  );

  return (
    <table layout="noBorders">
      {fieldNames.map((fieldName) => (
        <row>
          <cell>
            <text color="#12130e">
              {/* try to find the fieldTitle, if that fails, use the fieldName */}
              {POs.find((PO) => PO.$properties[fieldName])?.$properties[
                fieldName
              ]!.title || fieldName}
            </text>
          </cell>

          {POs.flatMap((PO) => {
            assertSafeProperty(fieldName);

            // if we're sharing multiple POs in this schema, and this field doesn't exist on this paticular PO,
            // then just render some empty cells
            if (!PO.$properties[fieldName]) {
              return [<cell> </cell>, <cell> </cell>];
            }

            const { value, formatted_value, verification } =
              PO.$properties[fieldName]!;
            const color = verifyColour(verification);

            // jsx-pdf currently doesn't support fragments, will make a PR at some point
            return [
              <cell>
                <FieldVerificationBadge width={10} status={verification} />
              </cell>,
              <cell>
                <InnerTableRows
                  key={fieldName}
                  value={formatted_value || value}
                  files={files}
                  config={config}
                  color={color}
                  imageFieldNames={imageFieldNames}
                />
              </cell>,
            ];
          })}
        </row>
      ))}
    </table>
  );
};
