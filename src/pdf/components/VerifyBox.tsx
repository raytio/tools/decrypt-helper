import JsxPdf from "jsx-pdf";
import type { AId, IId } from "@raytio/types";
import { $$ } from "../../locales/index.js";
import { NULL_I_ID } from "../constants.js";

type Props = {
  aId: AId;
  iId: IId;
  clientUrl: string;
};

export const VerifyBox = ({ aId, iId, clientUrl }: Props) => {
  if (iId === NULL_I_ID) return null;

  const url = `${clientUrl}/apps/s/${aId}/i/${iId}`;
  return (
    <stack alignment="center">
      <text link={url}>{$$("VerifyBox.text")}</text>
      <qr content={url} fit={150} />
      <text color="#1e90ff" fontSize={9} link={url} alignment="left">
        {url}
      </text>
    </stack>
  );
};
