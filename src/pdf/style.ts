import type { StyleDictionary } from "pdfmake/interfaces";
import { asset } from "./helpers/general.js";

export const classes: StyleDictionary = {
  header: {
    fontSize: 18,
  },
  listing: {
    fontSize: 10,
    color: "#333",
  },

  // [l, t, r, b]
  marginBottom: { margin: [0, 0, 0, 10] },
  marginY: { margin: [0, 15, 0, 15] },
};

export const fonts = {
  Lato: {
    normal: asset("Lato-Light.ttf"),
    bold: asset("Lato-Bold.ttf"),
    italics: asset("Lato-LightItalic.ttf"),
    bolditalics: asset("Lato-BoldItalic.ttf"),
  },
};
