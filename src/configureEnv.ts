// for @aws-amplify/auth
import "localstorage-polyfill";

// Polyfill crypto for node ≤18.
if (!globalThis.crypto) {
  (async () => {
    const { webcrypto } = await import("node:crypto");
    // @ts-expect-error fix
    globalThis.crypto = webcrypto;
  })();
}
