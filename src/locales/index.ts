import { readFileSync } from "node:fs";
import { join } from "node:path";
import { getDirname } from "../helpers/pathUtils.js";

const en = JSON.parse(
  readFileSync(join(getDirname(), "../locales/translations/en.json"), "utf8")
);

const locales = <const>{ en };

export type I18nKey = keyof typeof en;
export type I18nLang = keyof typeof locales;

declare global {
  // eslint-disable-next-line vars-on-top, no-var -- for some reason this has to be a var
  var lang: I18nLang;
}

export const isValidLocale = (lang: string): lang is I18nLang =>
  lang in locales;

export const $$ = (
  key: I18nKey,
  variables?: Record<string, string | number>
): string => {
  const strings = locales[global.lang || "en"];

  const replaceWithVariable = (_: unknown, variableName: string) => {
    const value = variables?.[variableName];
    if (value === undefined) {
      throw new TypeError(`[i18n] variable '${variableName}' not defined`);
    }

    return `${value}`;
  };

  return strings[key].replaceAll(/{([^}]+)}/g, replaceWithVariable);
};
