import type { IId, InstanceWithoutData } from "@raytio/types";
import { authedFetch } from "./authedFetch.js";
import type { EnvConfig } from "./fetchEnvConfig.js";

export async function fetchInstanceData(
  apiToken: string,
  envConfig: EnvConfig,
  instanceId: IId
): Promise<InstanceWithoutData> {
  const [instance] = await authedFetch<[InstanceWithoutData]>(
    apiToken,
    `${envConfig.api_url}/db/v1/dsm_access_application_instances?id=eq.${instanceId}`
  );
  return instance;
}
