import { authedFetch } from "./authedFetch.js";
import type { EnvConfig } from "./fetchEnvConfig.js";

//
// this file is mostly copy-pasted from the client repo. If you make
// a change here, consider making the same change in the client.
//

export async function uploadToObjectStore(
  apiToken: string,
  envConfig: EnvConfig,
  dataUrl: string,
  expiryDate?: Date
): Promise<{ objectStoreId: string; retrievalUrl: string }> {
  const mimeType = dataUrl.split(";")[0]!.split(":")[1]!;

  const base64 = dataUrl.split(",")[1]!;

  // eslint-disable-next-line unicorn/prefer-code-point -- deliberate, this will only ever be ASCII
  const arrayBuffer = Uint8Array.from(atob(base64), (c) => c.charCodeAt(0));

  // this is a weird API that returns a double stringified string
  const temporaryUrl = await authedFetch<string>(
    apiToken,
    `${envConfig.api_url}/org/v1/object/url${
      expiryDate ? `?expires=${+expiryDate}` : ""
    }`
  );

  const { status, statusText } = await fetch(temporaryUrl, {
    method: "PUT",
    body: arrayBuffer,
    headers: { "Content-Type": mimeType },
  });

  if (status !== 200) {
    throw new Error(`Status ${status} from object store: ${statusText}`);
  }

  // the v4 API doesn't return the ID
  const { pathname, origin } = new URL(temporaryUrl);

  return {
    objectStoreId: pathname.slice(1),
    retrievalUrl: origin + pathname,
  };
}
