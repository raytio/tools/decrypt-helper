import type { NId } from "@raytio/types";
import type { EnvConfig } from "../fetchEnvConfig";
import {
  clearCache,
  resolveVerificationDetails,
} from "../resolveVerificationDetails";

const envConfig = <EnvConfig>{ clientUrl: "https://example.com" };

describe("resolveVerificationDetails", () => {
  it("can resolve the name of a verifier", async () => {
    clearCache();
    Reflect.set(
      globalThis,
      "fetch",
      vi.fn(async () => ({
        json: async () => ({
          verifiers: {
            avsec: { name: "Kaiwhakamaru Rererangi" },
            aklc: { name: "Te Kaunihera o Tāmaki Makaurau" },
          },
        }),
      }))
    );

    expect(
      await resolveVerificationDetails(
        [{ serviceProviderNId: <NId>"avsec", verifierNId: <NId>"Unknown" }],
        envConfig
      )
    ).toStrictEqual({
      date: undefined,
      verifier_id: undefined,
      verifier_service_id: "Kaiwhakamaru Rererangi",
      verifier_source_id: undefined,
    });
  });

  it("does not crash if the verifier list cannot be downloaded", async () => {
    clearCache();
    Reflect.set(globalThis, "fetch", () => Promise.reject());

    expect(
      await resolveVerificationDetails(
        [{ serviceProviderNId: <NId>"avsec", verifierNId: <NId>"Unknown" }],
        envConfig
      )
    ).toStrictEqual({
      date: undefined,
      verifier_id: undefined,
      verifier_service_id: undefined,
      verifier_source_id: undefined,
    });
  });
});
