import { fetchEnvConfig } from "../fetchEnvConfig";

describe.each`
  clientUrl                    | apiUrl
  ${"https://app.rayt.io"}     | ${"https://api.rayt.io"}
  ${"https://app-dev.rayt.io"} | ${"https://api-dev.rayt.io"}
`("fetchEnvConfig $clientUrl / $apiUrl", ({ clientUrl, apiUrl }) => {
  it("works if you pass it a valid client url", async () => {
    expect(await fetchEnvConfig(clientUrl)).toStrictEqual({
      api_url: apiUrl,
      cognito_region: "us-east-1",
      cognito_user_pool_id: expect.any(String),
      cognito_web_client_id: expect.any(String),
      app_name: "Raytio",
      logo_url: undefined,
      clientUrl,
    });
  });

  it("throws an error if you pass it an invalid url", async () => {
    await expect(() => fetchEnvConfig(apiUrl)).rejects.toThrow(
      new Error(
        `The CLIENT_URL you specified is not a valid Raytio client. Are you sure you used the client URL, not the API url? You supplied: "${apiUrl}" (Error: Missing: cognito_region, cognito_user_pool_id, cognito_web_client_id, api_url)`
      )
    );
  });
});
