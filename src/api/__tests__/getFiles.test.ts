import type { IId, Instance, NId, NId2, ProfileObject } from "@raytio/types";
import { authedFetch } from "../authedFetch";
import type { EnvConfig } from "../fetchEnvConfig";
import { getFiles } from "../getFiles";

vi.mock("../authedFetch");
vi.mock("../videoToImage", () => ({
  videoToImage: async () => "data:image/jpeg;...this is a frame from a video",
}));

const profileObjects: ProfileObject[] = [
  {
    // test case A & D
    id: <NId2>"n1",
    n_id: <NId>"n1",
    properties: {
      name: "sam sample",
      age: 123,
      photo: {
        content: `urn:temp_object:${Buffer.from(
          "https://example.com/foto.jpg",
          "binary"
        ).toString("base64")}`,
        n_id: "my_image_1",
      },
    },
    labels: ["ss_driver_license"],
  },
  {
    // test case B & D
    id: <NId2>"n2",
    n_id: <NId>"n2",
    properties: {
      name: "Peter Sam",
      age: 105,
      photo: {
        content: { encrypted_data: {}, encrypted_key: {} },
        n_id: "my_image_2",
      },
    },
    labels: ["ss_driver_license"],
  },
  {
    // test case C & D
    id: <NId2>"n3",
    n_id: <NId>"n3",
    properties: {
      name: "Peter Sam 🚂",
      photo: { content: "urn:image:my_image_3", n_id: "my_image_3" },
    },
    labels: ["ss_driver_license"],
  },
  {
    // test case D. Filtered out because it is encrypted but there are no keys
    id: <NId2>"n4",
    n_id: <NId>"n4",
    properties: {
      name: "Peter Sam 🚂",
      photo: {
        content: { encrypted_data: {}, encrypted_key: {} },
        n_id: "my_image_4",
      },
    },
    labels: ["ss_driver_license"],
  },
  {
    // test case D. Filtered out because the file contains no data
    id: <NId2>"n5",
    n_id: <NId>"n5",
    properties: {
      name: "Peter Sam 🚂",
      photo: { content: "urn:image:my_image_4", n_id: "my_image_4" },
    },
    labels: ["ss_driver_license"],
  },
  {
    // test case E. Video
    id: <NId2>"n6",
    n_id: <NId>"n6",
    properties: {
      video: { content: "urn:image:my_video_n6", n_id: "my_video_n6" },
    },
    labels: ["ss_driver_license"],
  },
];

describe("getFiles", () => {
  beforeEach(() => {
    vi.clearAllMocks();

    m(authedFetch).mockImplementation(async (_: string, url: string) =>
      Buffer.from(
        // eslint-disable-next-line no-nested-ternary
        url.includes("my_image_4")
          ? ""
          : url.includes("video")
            ? "data:video/x-matroska;base64,vee dee oh"
            : "data:image/png;base64,PeeEnGee",
        "binary"
      ).toString("base64")
    );
    Reflect.set(
      globalThis,
      "fetch",
      vi.fn(async () => ({
        arrayBuffer: async () => new TextEncoder().encode("JayPeeGee"),
        headers: { get: () => "image/jpeg" },
      }))
    );
  });

  const instance = {
    id: <IId>"i1",
    keys: { n2: { photo: { data: Symbol.for("wdek") } } },
  } as unknown as Instance;

  const envConfig = { api_url: "https://api.example.net" } as EnvConfig;
  const applicationDecryptor = {
    decrypt: async () => "data:image/gif;base64,ThisIsADecryptedJif",
  };

  it("(A) can fetch files from urn:temp_object:", async () => {
    const files = await getFiles(
      [profileObjects[0]!],
      instance,
      "api token",
      envConfig,
      applicationDecryptor
    );

    expect(files).toStrictEqual({
      my_image_1: ["data:image/jpeg;base64,SmF5UGVlR2Vl", "jpeg"],
    });

    // because this test case uses the temp-obj system
    expect(authedFetch).not.toHaveBeenCalled();

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith("https://example.com/foto.jpg");
  });

  it("(B) can handle encrypted normal files", async () => {
    const files = await getFiles(
      [profileObjects[1]!],
      instance,
      "api token",
      envConfig,
      applicationDecryptor
    );

    expect(files).toStrictEqual({
      my_image_2: ["data:image/gif;base64,ThisIsADecryptedJif", "gif"],
    });

    // because this test case uses the non-temp-obj system
    expect(fetch).not.toHaveBeenCalled();

    expect(authedFetch).toHaveBeenCalledTimes(1);
    expect(authedFetch).toHaveBeenCalledWith(
      "api token",
      "https://api.example.net/share/v2/access_application/instance/i1/profile_object/my_image_2/content"
    );
  });

  it("(C) can handle unencrypted normal files", async () => {
    const files = await getFiles(
      [profileObjects[2]!],
      instance,
      "api token",
      envConfig,
      applicationDecryptor
    );

    expect(files).toStrictEqual({
      my_image_3: ["data:image/png;base64,PeeEnGee", "png"],
    });

    // because this test case uses the non-temp-obj system
    expect(fetch).not.toHaveBeenCalled();

    expect(authedFetch).toHaveBeenCalledTimes(1);
    expect(authedFetch).toHaveBeenCalledWith(
      "api token",
      "https://api.example.net/share/v2/access_application/instance/i1/profile_object/my_image_3/content"
    );
  });

  it("(D) filters out broken or undefined images", async () => {
    const files = await getFiles(
      profileObjects.slice(0, 5), // this test case contains examples n1-n5
      instance,
      "api token",
      envConfig,
      applicationDecryptor
    );

    expect(files).toStrictEqual({
      my_image_1: ["data:image/jpeg;base64,SmF5UGVlR2Vl", "jpeg"],
      my_image_2: ["data:image/gif;base64,ThisIsADecryptedJif", "gif"],
      my_image_3: ["data:image/png;base64,PeeEnGee", "png"],
      // only these 3 are included in the final response
    });
  });

  it("(E) adds a static frame for every video in the output", async () => {
    const files = await getFiles(
      [profileObjects[5]!], // only n6
      instance,
      "api token",
      envConfig,
      applicationDecryptor
    );

    expect(files).toStrictEqual({
      my_video_n6: ["data:video/x-matroska;base64,vee dee oh", "mkv"],
      my_video_n6_videoFrame: [
        "data:image/jpeg;...this is a frame from a video",
        "jpeg",
      ],
    });
  });
});
