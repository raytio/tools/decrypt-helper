import type { NId, VerificationProvider } from "@raytio/types";
import type { EnvConfig } from "./fetchEnvConfig.js";

export type ResolvedVerificationProvider = {
  verifier_id?: string;
  verifier_service_id?: string;
  verifier_source_id?: string;
  date?: string;
};

type VerifierList = { verifiers: { [nId: NId]: { name: string } } };

let verifiersPromise: Promise<VerifierList> | null;

export async function resolveVerificationDetails(
  [details]: VerificationProvider[],
  envConfig: EnvConfig
): Promise<ResolvedVerificationProvider | undefined> {
  if (!details) return undefined;

  verifiersPromise ||= fetch(`${envConfig.clientUrl}/verifiers.json`)
    .then((r) => r.json())
    .catch(() => ({ verifiers: {} }));

  const { verifiers } = await verifiersPromise;

  return {
    verifier_id: verifiers[details.verifierNId!]?.name,
    verifier_service_id: verifiers[details.serviceProviderNId!]?.name,
    verifier_source_id: verifiers[details.dataSourceNId!]?.name,
    date: details.date?.toISOString(),
  };
}

/** @deprecated - for use in unit tests only */
export const clearCache = () => {
  verifiersPromise = null;
};
