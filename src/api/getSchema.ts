import type { Schema, WrappedSchema } from "@raytio/types";
import { expandSchema } from "@raytio/core";
import { authedFetch } from "./authedFetch.js";
import type { EnvConfig } from "./fetchEnvConfig.js";

export async function getAllSchema(envConfig: EnvConfig): Promise<Schema[]> {
  const list = await authedFetch<WrappedSchema[]>(
    "",
    `${envConfig.api_url}/db/v1/dsm_schema?version_current=eq.true`
  );
  return list.map((wrappedSchema) =>
    expandSchema(wrappedSchema, list, [
      process.env.PDF_LANGUAGE || process.env.DATE_FORMAT || "en-NZ",
    ])
  );
}
