import { Auth, type CognitoUser } from "@aws-amplify/auth";
import { hashPassword } from "@raytio/core";
import type { ICognitoUserAttributeData } from "amazon-cognito-identity-js";
import type { MfaRequiredDetails } from "@aws-amplify/auth/lib-esm/types";
import type { Config } from "../constants.js";
import type { EnvConfig } from "./fetchEnvConfig.js";

type SignInResponse = CognitoUser &
  Partial<MfaRequiredDetails> & {
    signInUserSession?: { accessToken?: { jwtToken: string } };
  };

/** see #1252 in the client repo */
async function signInWithPasswordMigration(
  username: string,
  password: string
): Promise<SignInResponse> {
  try {
    const userObject: SignInResponse = await Auth.signIn(username, password);
    return userObject;
  } catch {
    // if the login fails, try again with their hashed password.
    // if it's successful the second time, we quietly change their password.
    const hashedPassword = await hashPassword(password);

    const userObject: SignInResponse = await Auth.signIn(
      username,
      hashedPassword
    );

    // the login was successful. So we need to migrate their account.
    // No changes to the maxcryptor, purely to cognito.

    // we can only migrate their password if there are no login challenges
    if (!userObject.challengeName) {
      console.log("Migrating credentials...");
      await Auth.changePassword(userObject, hashedPassword, password);
    }

    return userObject;
  }
}

export async function signIn(
  CONFIG: Config,
  envConfig: EnvConfig
): Promise<{
  apiToken: string;
  cognitoAttributes: ICognitoUserAttributeData[];
}> {
  Auth.configure({
    region: envConfig.cognito_region,
    userPoolId: envConfig.cognito_user_pool_id,
    userPoolWebClientId: envConfig.cognito_web_client_id,
  });

  const userObject = await signInWithPasswordMigration(
    CONFIG.RAYTIO_USERNAME,
    CONFIG.RAYTIO_PASSWORD
  );
  if (userObject.challengeName === "SOFTWARE_TOKEN_MFA") {
    throw new Error(
      `The configured account (${CONFIG.RAYTIO_USERNAME}) has two factor authentication enabled. You must disable 2FA or use a different account`
    );
  }
  const user = await Auth.currentAuthenticatedUser();
  const apiToken = user.signInUserSession?.accessToken?.jwtToken;

  const cognitoAttributes = await Auth.userAttributes(user);

  return { apiToken, cognitoAttributes };
}
