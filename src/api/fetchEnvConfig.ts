const PARAMS = [
  "cognito_region",
  "cognito_user_pool_id",
  "cognito_web_client_id",
  "api_url",
  "app_name", // whitelabelling name
  "logo_url", // must be a URL to a png file
] as const;

export type EnvConfig = Record<(typeof PARAMS)[number], string> & {
  clientUrl: string;
};

const DEFAULTS: Partial<EnvConfig> = {
  app_name: "Raytio",
  logo_url: undefined,
};

export async function fetchEnvConfig(clientUrl: string): Promise<EnvConfig> {
  try {
    const envConfig: EnvConfig = {
      ...DEFAULTS,
      ...(await fetch(`${clientUrl}/client_config.json`).then((r) => r.json())),
    };

    const missing = PARAMS.filter((p) => !(p in envConfig));
    if (missing.length) throw new Error(`Missing: ${missing.join(", ")}`);

    return {
      ...envConfig,
      clientUrl,
    };
  } catch (ex) {
    throw new Error(
      `The CLIENT_URL you specified is not a valid Raytio client. Are you sure you used the client URL, not the API url? You supplied: "${clientUrl}" (${ex})`
    );
  }
}
