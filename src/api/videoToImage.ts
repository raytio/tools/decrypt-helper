import { authedFetch } from "./authedFetch.js";
import type { EnvConfig } from "./fetchEnvConfig.js";
import { uploadToObjectStore } from "./uploadToObjectStore.js";

/** Use the extractor API to get the middle frame from a video */
export async function videoToImage(
  apiToken: string,
  envConfig: EnvConfig,
  videoDataUrl: string
): Promise<string> {
  // the new API can't handle big dataUrls, so we need to store the data in
  // the object store first...
  const in10Mins = new Date();
  in10Mins.setMinutes(in10Mins.getMinutes() + 10);
  const { retrievalUrl: videoUrl } = await uploadToObjectStore(
    apiToken,
    envConfig,
    videoDataUrl,
    in10Mins
  );

  const extractedImageBase64 = await authedFetch<string>(
    apiToken,
    `${envConfig.api_url}/face-detect/v1/image-extract-single`,
    // note: inspite of being called "video_urn", the new API actually expects a url, not a urn...
    { method: "POST", body: JSON.stringify({ video_urn: videoUrl }) }
  );

  // annoyingly the new API doesn't return the data URL prefix, so we have to hardcode it here
  return `data:image/jpeg;base64,${extractedImageBase64}`;
}
