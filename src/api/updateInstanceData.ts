import type { IId, InstanceWithoutData } from "@raytio/types";
import { authedFetch } from "./authedFetch.js";
import type { EnvConfig } from "./fetchEnvConfig.js";

/**
 * only specific fields can be updated, passing the entire
 * instance object to the v3 API will cause an error.
 */
export async function updateInstanceData(
  apiToken: string,
  envConfig: EnvConfig,
  iId: IId,
  instance: Partial<Pick<InstanceWithoutData, "state" | "score" | "metadata">>
): Promise<void> {
  await authedFetch<[InstanceWithoutData]>(
    apiToken,
    `${envConfig.api_url}/db/v1/dsm_access_application_instances_u?id=eq.${iId}`,
    { method: "PATCH", body: JSON.stringify(instance) }
  );
}
