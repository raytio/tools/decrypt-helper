import type { Lookup } from "@raytio/types";
import { authedFetch } from "./authedFetch.js";

/**
 * We cache the promise so that only one XHR request is ever sent
 */
const cache = new Map<string, Promise<Lookup[]>>();

export async function getLookupOption(
  apiToken: string,
  lookup: string
): Promise<Lookup[]> {
  const { origin } = new URL(lookup);

  const cached = cache.get(lookup);
  if (cached) return cached;

  // if it's a lookup to our own API, then include the IdentityToken
  const promise: Promise<Lookup[]> = origin.endsWith(".rayt.io")
    ? authedFetch(apiToken, lookup)
    : fetch(lookup).then((r) => r.json());

  cache.set(lookup, promise);

  return promise;
}
