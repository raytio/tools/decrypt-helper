export async function authedFetch<T>(
  apiToken: string,
  url: string,
  options?: RequestInit,
  retrying = false
): Promise<T> {
  console.log(`[API] ${retrying ? "Retry" : "Start"} ${url}`);

  const startTime = Date.now();
  const response = await fetch(url, {
    ...options,
    headers: { Authorization: `Bearer ${apiToken}` },
  });
  const apiResponse = await response.json();

  const error = apiResponse.message || apiResponse.error;
  if (error) {
    if (!retrying && response.status === 504) {
      console.log(`[API] Error ${response.status} (will retry) ${url}`);
      return authedFetch(apiToken, url, options, true);
    }

    console.log(`[API] Error ${response.status} (no retry) ${url}`);

    throw new Error(`Failed due to API Error from ${url}: "${error}"`);
  }

  const totalTime = ((Date.now() - startTime) / 1000).toFixed(1);

  console.log(
    `[API] Finish${retrying ? " after retry" : ""} (${totalTime}s) ${url}`
  );

  return apiResponse;
}
