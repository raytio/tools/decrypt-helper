import type { AA, AId } from "@raytio/types";
import { authedFetch } from "./authedFetch.js";
import type { EnvConfig } from "./fetchEnvConfig.js";

type ServerAA = Omit<AA, "a_id"> & { id: AId };

export async function fetchAA(
  apiToken: string,
  envConfig: EnvConfig,
  aId: AId
): Promise<AA> {
  const [AA] = await authedFetch<ServerAA[]>(
    apiToken,
    `${envConfig.api_url}/db/v1/dsm_access_applications?id=eq.${aId}`
  );
  if (!AA) throw new Error("AA not found");
  return { ...AA, a_id: AA.id };
}
