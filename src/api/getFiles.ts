import { getNidFromUrn, isEncrypted } from "@raytio/core";
import { extension, types } from "mime-types";
import type {
  Encrypted,
  Instance,
  NId,
  ProfileObject,
  Urn,
} from "@raytio/types";
import { isFieldValueFile, isTruthy, isUrn } from "../helpers/file.js";
import type { ApplicationEncryptorLike } from "../types.js";
import { authedFetch } from "./authedFetch.js";
import type { EnvConfig } from "./fetchEnvConfig.js";
import { videoToImage } from "./videoToImage.js";

const TEMP_OBJ_PREFIX = "urn:temp_object:";

const getFileExtn = (b64: string): string =>
  extension(b64.split(":")[1]?.split(";base64,")[0] || "text/plain") || "txt";

const decryptFile = async (
  encryptedData: string,
  encryptedObject: Encrypted,
  applicationDecryptor: ApplicationEncryptorLike,
  wdek: string
): Promise<string> => {
  const clonedEncryptedObject = JSON.parse(JSON.stringify(encryptedObject));
  clonedEncryptedObject.encrypted_data.data = encryptedData;

  const decrypted = await applicationDecryptor.decrypt(
    clonedEncryptedObject,
    wdek
  );
  return decrypted as string;
};

const cleanApiResponse = (responseBody: string): string => {
  try {
    const realB64 = Buffer.from(responseBody, "base64").toString("binary");
    if (realB64.slice(0, 5) === "data:") {
      return realB64;
    }
  } catch {
    // for some reason we need to do this...
  }
  return responseBody;
};

type FileObject = Record<NId, [dataUrl: string, fileExtension: string]>;

function tryGetFallbackUrn(urnOrEncrypted: Urn | Encrypted): NId | undefined {
  const urn = isEncrypted(urnOrEncrypted)
    ? urnOrEncrypted.encrypted_data.data // no nessesaily a Urn
    : urnOrEncrypted;

  return isUrn(urn) ? getNidFromUrn(urn) : undefined;
}

export async function getFiles(
  profileObjects: ProfileObject[],
  instance: Instance,
  apiToken: string,
  envConfig: EnvConfig,
  applicationDecryptor: ApplicationEncryptorLike
): Promise<FileObject> {
  // [nId2: NId2, fieldName: string, file: Urn | Encrypted<Urn>][]
  const urnOrEncryptedList = profileObjects.flatMap((PO) => {
    return Object.entries(PO.properties)
      .map(([k, v]) => {
        // this value is not a file, so abort
        if (!isFieldValueFile(v)) return undefined;

        const urnOrEncrypted = "content" in v ? v.content : v.Content;

        /**
         *
         * ```ts
         * // pre-2021 data structure:
         * [
         *  { n_id: 1, properties: { field1: File }
         * ]
         *
         * // post-2021 data structure:
         * [
         *   { n_id: 1, properties: { field1: { $ref: 2 } } },
         *   { n_id: 2, properties: File }
         * ]
         * ```
         *
         * In 2023, the API was changed so that properties (in this
         * case field1) so longer include the n_id. For the pre-2021
         * example, this means we have no official way of getting the
         * n_id of the file content.
         *
         * To continue supporting the legacy data, we have an escape
         * hatch: {@link tryGetFallbackUrn} to look at the internal
         * structure of the File object and (try) to get the Urn.
         *
         * This won't work for extremely old files (pre 2020), since
         * we used to store the entire data url in the PO, there were
         * no Urns or refs at that point.
         */
        const fileNId = v.n_id || tryGetFallbackUrn(urnOrEncrypted);

        // if we couldn't figure out the file content's nId, abort.
        if (!fileNId) return undefined;

        return <const>[PO.id!, fileNId, k, urnOrEncrypted];
      })
      .filter(isTruthy);
  });

  const filesBase64 = await Promise.all(
    urnOrEncryptedList.map(
      async ([PONId2, fileNId, fieldName, urnOrEncrypted]): Promise<
        [fileNId: NId, file: undefined | [dataUrl: string, fileExtn: string]]
      > => {
        // handle urn:temp_object:
        if (
          typeof urnOrEncrypted === "string" &&
          urnOrEncrypted.startsWith(TEMP_OBJ_PREFIX)
        ) {
          const url = Buffer.from(
            urnOrEncrypted.slice(TEMP_OBJ_PREFIX.length),
            "base64"
          ).toString("binary");
          const response = await fetch(url);
          const base64 = Buffer.from(await response.arrayBuffer()).toString(
            "base64"
          );
          const type = response.headers.get("content-type") || "text/plain";
          const dataUrl = `data:${type};base64,${base64}`;

          return [fileNId, [dataUrl, getFileExtn(dataUrl)]];
        }

        const realDataUrl = await authedFetch<string>(
          apiToken,
          `${envConfig.api_url}/share/v2/access_application/instance/${instance.id}/profile_object/${fileNId}/content`
        ).then(cleanApiResponse);

        if (isEncrypted(urnOrEncrypted)) {
          const wdek = instance.keys[PONId2]?.[fieldName]?.data;
          if (!wdek) return [fileNId, undefined];

          const decryptedDataUrl = await decryptFile(
            realDataUrl,
            urnOrEncrypted,
            applicationDecryptor,
            wdek
          );
          return [fileNId, [decryptedDataUrl, getFileExtn(decryptedDataUrl)]];
        }
        return [fileNId, [realDataUrl, getFileExtn(realDataUrl)]];
      }
    )
  );

  const allDataUrls: FileObject = Object.fromEntries(
    filesBase64.filter(
      (file): file is [NId, [string, string]] =>
        !!file[1] && file[1][0]?.includes(",")
    )
  );

  // for all videos, also store a static frame from the video, since we can't embedded a video into the PDF
  for (const nId in allDataUrls) {
    const [dataUrl, fileExtn] = allDataUrls[<NId>nId]!;
    const isVideo = types[fileExtn]?.startsWith("video/");
    if (isVideo) {
      const imageDataUrl = await videoToImage(apiToken, envConfig, dataUrl);
      allDataUrls[<NId>`${nId}_videoFrame`] = [
        imageDataUrl,
        getFileExtn(imageDataUrl),
      ];
    }
  }

  return allDataUrls;
}
