/**
 * This is just for developing locally
 */

import { createWriteStream, readFileSync } from "node:fs";
import { join } from "node:path";
import PdfPrinter from "pdfmake";
import JsxPdf from "jsx-pdf";
import type { AId } from "@raytio/types";
import { Report } from "../pdf/components/Report";
import { fonts } from "../pdf/style";
import { getDirname } from "../helpers/pathUtils";
import type { EnvConfig } from "../api";

let json;
try {
  json = JSON.parse(
    readFileSync(join(getDirname(), "../../out.json"), { encoding: "utf8" })
  );
} catch {
  console.error(
    "You need to save some data into out.json beforing using this test script"
  );
  process.exit(1);
}

let files = {};
try {
  files = JSON.parse(
    readFileSync(join(getDirname(), "../../out-otherfiles.json"), {
      encoding: "utf8",
    })
  );
} catch {
  console.warn("Files not found");
}

const printer = new PdfPrinter(fonts);

const pdfDocument = printer.createPdfKitDocument(
  JsxPdf.renderPdf(
    <Report
      data={json}
      files={files}
      aId={"my_a_id" as AId}
      clientUrl="https://example.com"
      config={{ DATE_FORMAT: "fr-FR", TIMEZONE: "Atlantic/Azores" }}
      envConfig={{ app_name: "Raytio" } as EnvConfig}
    />
  )
);

pdfDocument.pipe(createWriteStream("out.pdf"));
pdfDocument.end();
