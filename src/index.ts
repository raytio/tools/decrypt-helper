// do this first
import "./configureEnv.js";

/* this is what consumers of the package have access to */

export { fetchEnvConfig, EnvConfig } from "./api/fetchEnvConfig.js";
export { generatePDF } from "./public-methods/generatePDF.js";
export { processSubmission } from "./public-methods/processSubmission.js";
export { getAndValidateConfig } from "./public-methods/getAndValidateConfig.js";
export { saveToS3Bucket } from "./public-methods/saveToS3Bucket.js";
export { version } from "./public-methods/version.js";
