# Decrypt Helper

![pipeline](https://gitlab.com/raytio/tools/decrypt-helper/badges/main/pipeline.svg?style=flat-square)
![coverage](https://gitlab.com/raytio/tools/decrypt-helper/badges/main/coverage.svg?style=flat-square)
![npm version](https://img.shields.io/npm/v/@raytio/decrypt-helper?color=%234c1&style=flat-square)
[![install size](https://packagephobia.com/badge?p=@raytio/decrypt-helper)](https://packagephobia.com/result?p=@raytio/decrypt-helper)

## Install

This SDK is designed to run on nodejs >=18.

```sh
npm install --save @raytio/decrypt-helper

# or if you use yarn:
yarn add @raytio/decrypt-helper
```

## General Usage

To process a submission, you need to supply an `applicationId`, `instanceId`, and `config`.

```js
const raytio = require("@raytio/decrypt-helper");

raytio
  .processSubmission({
    instanceId: "...",
    applicationId: "...",
    config: {
      // the URL to the raytio client (not the API)
      CLIENT_URL: "https://app.rayt.io",

      // if using raytio.saveToS3Bucket, specify the ID of the bucket here
      S3_BUCKET: "",

      // the credentials of an organization user
      RAYTIO_USERNAME: "...",
      RAYTIO_PASSWORD: "...",

      // used when generating a PDF report, if not supplied defaults to "en-NZ" and "Pacific/Auckland"
      // for available timezone options, see the map on https://momentjs.com/timezone
      DATE_FORMAT: "en-nz",
      TIMEZONE: "Pacific/Auckland",
      PDF_LANGUAGE: "en", // the value must be one of the languages listed here: https://gitlab.com/raytio/tools/decrypt-helper/-/tree/main/src/locales/translations
    },
    verbose: true,
  })
  .then(raytio.generatePDF()) // optional, will add the report to `files` in the output
  .then(({ json, csv, files }) => {
    // do something with `json`, `csv`, and `files`
  });
```

## Usage in AWS Lambda

When running in AWS Lambda, you can use the function `raytio.getAndValidateConfig()` to extract the configuration from the serverless.yml file.

You can also pipe the response directly to `raytio.saveToS3Bucket`.

```js
const raytio = require("@raytio/decrypt-helper");

raytio
  .processSubmission({
    instanceId: "...",
    applicationId: "...",
    config: raytio.getAndValidateConfig(),
    verbose: true,
  })
  .then(raytio.generatePDF())
  .then(raytio.saveToS3Bucket());
```

# Help

For more infomation, please refer to the [Raytio Documentation](https://dev-docs.rayt.io)

## Contributing

To test the PDF generation, run `yarn dev-pdf`

### Regression tests

We use jest for unit tests, snapshot tests of the PDF, and integration tests for whole process. See the [tests](./tests) folder.

To run tests, use `yarn test`. To run and update snapshots, use `yarn test -u`. You will need to configure the environment variables in `.env` for integration tests.

If jest apears to be stuck running the end-to-end tests, run `yarn jest --clearCache`.

### Manual testing

You can also test changes to this code using serverless-offline, [see here for more info](https://gitlab.com/raytio/common/pdf-generate/-/tree/main/packages/pdf-generate#testing).
