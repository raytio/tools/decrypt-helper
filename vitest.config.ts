import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    globals: true,
    setupFiles: ["./src/__tests__/setupTests.ts"],
    mockReset: true,
    clearMocks: true,

    environment: "node",
    testTimeout: 300000,
    // See https://github.com/vitest-dev/vitest/issues/740
    // and https://github.com/Automattic/node-canvas/issues/1394
    pool: "forks",

    reporters: [
      "default",
      "jest-image-snapshot/src/outdated-snapshot-reporter.js",
      ...(process.env.CI ? ["junit"] : []),
    ],
    outputFile: {
      junit: "tmp/junit.xml",
    },
    coverage: {
      provider: "v8",
      all: true,
      include: ["src/**"],
      reporter: ["html", "text-summary", "clover", "lcov"],
    },
  },
});
